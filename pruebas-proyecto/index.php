<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FilmRate</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
 
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="css/estilos.css"/>
    
</head>
<body>
    
    <?php 
        require "cabeceras/cabeceraPublica.php";
    ?>

    <div id="contenidoPublico">        
        <div id="divPublico" class="container text-dark rounded shadow">            
        ESTE CONTENIDO SERÁ PUBLICO<br>
        
           
           
        </div> 
    </div>

    <div id="formulariologin" style="display:none">
        <div id="divLogin" class="container  text-dark rounded shadow">
            FORMULARIO LOGIN
        </div>
    </div>

    </div>
    <div id="formularioregistrarse" style="display:none">
        <div id="divRegistro" class="container text-dark rounded shadow">
            formularioregistrarse
        </div>        
    </div>
   

    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="jquery/jquery.js"></script>
    <!--Popper-->   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!--BOOTSTRAP-->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->
    <!-- CDN de Vue -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>-->      
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
  
    <?php
        require "footer/footer.php";
    ?>

    <script src="/main.js"></script>

</body>

</html>