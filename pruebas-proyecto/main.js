$(document).ready(function(){
    //Al cargar la pagina oculto los formularios de iniciar y registrarse
    $("#formularioregistrarse").hide();
    $("#formulariologin").hide();


    //Al hacer click en "iniciar sesion" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#iniciarsesion").on("click",function(){
        $("#contenidoPublico").hide();
        $("#formularioregistrarse").hide();
        $("#formulariologin").show();

    });

    //Al hacer click en "registrase" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#registrarse").on("click",function(){
        $("#contenidoPublico").hide();
        $("#formulariologin").hide();
        $("#formularioregistrarse").show();
    });


    //Al hacer click en "iniciarsesion" se muestra el formulario de inicio de sesion
    //y se oculta el resto de esta manera evito pantallas de carga
    $("#irainicio").on("click",function(){
        $("#contenidoPublico").show();
        $("#formulariologin").hide();
        $("#formularioregistrarse").hide();
    })

})