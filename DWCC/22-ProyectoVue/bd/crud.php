<?php
include_once 'conexion.php';


//Para recibir parametros con Axios
$_POST = json_decode(file_get_contents("php://input"),true);

//Recepcion de datos

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';//Esta variable describe lo que se hará al pulsar el boton

$id = (isset($_POST['id'])) ? $_POST['id'] : '';
$marca = (isset($_POST['marca'])) ? $_POST['marca'] : '';
$modelo = (isset($_POST['modelo'])) ? $_POST['modelo'] : '';
$stock = (isset($_POST['stock'])) ? $_POST['stock'] : '';
$precio = (isset($_POST['precio'])) ? $_POST['precio'] : '';

$marcaBuscar = (isset($_POST['marcaBuscar'])) ? $_POST['marcaBuscar'] : '';


//Evaluaremos la opcion con un switchcase
switch($opcion){
    case 1: //Nueva consola
        $consulta = "INSERT INTO consolas (marca,modelo,stock,precio) VALUES ('$marca','$modelo','$stock','$precio');";
        $resultado = $pdo -> prepare($consulta);
        $resultado->execute();
        break;
    case 2://modificación
        $consulta = "UPDATE consolas SET marca='$marca', modelo='$modelo', stock='$stock', precio='$precio' WHERE id='$id'";
        $resultado = $pdo -> prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3://BORRAR
        $consulta = "DELETE FROM consolas WHERE id='$id'";
        $resultado = $pdo -> prepare($consulta);
        $resultado->execute();
        break;
    case 4://Listar
        $consulta = "SELECT id,marca,modelo,stock,Round(precio,2)AS precio FROM consolas;";
        $resultado = $pdo -> prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 5://Listar por marca
        if($marcaBuscar==""){
            $consulta = "SELECT id,marca,modelo,stock,Round(precio,2)AS precio FROM consolas";
        }else{
            $consulta = "SELECT id,marca,modelo,stock,Round(precio,2)AS precio FROM consolas WHERE marca='$marcaBuscar'";
        }        
        $resultado = $pdo -> prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

//Enviamos el array en formato json a un fichero en JavaScript
print json_encode($data, JSON_UNESCAPED_UNICODE);
//Cerramos la conexion
$conexion=NULL;



?>
