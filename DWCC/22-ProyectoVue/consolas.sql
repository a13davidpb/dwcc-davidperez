-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Xerado en: 21 de Mar de 2020 ás 22:43
-- Versión do servidor: 10.3.22-MariaDB-0+deb10u1
-- Versión do PHP: 7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectoFinalVue`
--

-- --------------------------------------------------------

--
-- Estrutura da táboa `consolas`
--

CREATE TABLE `consolas` (
  `id` int(11) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A extraer os datos da táboa `consolas`
--

INSERT INTO `consolas` (`id`, `marca`, `modelo`, `stock`, `precio`) VALUES
(1, 'Nintendo', 'Switch', 3, 319.95),
(2, 'Sony', 'PlayStation 1', 10, 19.95),
(3, 'Sony', 'PlayStation 3', 15, 99),
(4, 'Nintendo', 'NES', 2, 50),
(5, 'Microsoft', 'Xbox 360', 11, 89.95),
(6, 'Microsoft', 'Xbox One', 100, 200),
(7, 'Sony', 'PlayStation 4', 100, 199.95);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consolas`
--
ALTER TABLE `consolas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consolas`
--
ALTER TABLE `consolas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
