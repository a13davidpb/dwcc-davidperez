const url = "bd/crud.php" //Ruta al crud.
var appConsolas = new Vue({
    el: "#appConsolas",
    data:{
        consolas:[],
        marca:"",
        modelo:"",
        stock:"",
        precio:"",
        ganancia:"",
        ganancias:0,
        total:0,
        

    },
    methods:{

        cargarPagina : function(){
            $(document).ready(function(){
                $(".btnMostrar").hide();
            
                //FUNCION PARA QUE FUNCIONE LO DE BUSCAR PULSANDO ENTER
                $("#marcaBuscar").keyup(function(evento){
                    if(evento.which==13){
                        //console.log("ENTER");
                        appConsolas.botonBuscarMarca();
                    }
                })           
            
            });
        },


        //BOTONES (async es para poder utilizar peticiones Ajax)
        botonNuevo: async function(){
          
            //Aqui utilizare sweetalert2
            const { value: formValues } = await Swal.fire({
                title: 'Agregar Consola',
                html:
                    `<br><div class="row"><label class="col-sm-3 col-form-label">Marca</label><div class="col-sm-7"><input id="marca" type="text" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Modelo</label><div class="col-sm-7"><input id="modelo" type="text" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Stock</label><div class="col-sm-7"><input id="stock" type="number" min="0" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Precio</label><div class="col-sm-7"><input id="precio" type="text" min="0" class="form-control"></div></div><br>
                    
                    `,
                focusConfirm: false,
                showCancelButton:true,
                confirmButtonText:'Añadir',
                cancelButtonText:'Cancelar',                
                confirmButtonColor:'#7CD825',
                cancelButtonColor:'#D85625',
                
                preConfirm: () => {
                    //Para corregir el error de la alerta con campos vacios cuando se le da a cancelar:
                    //Añadi mas condiciones, de esta manera es mas seguro a la hora de intentar introducir campos vacios.
                    if (document.getElementById('marca').value==""||
                        document.getElementById('modelo').value==""|| 
                        document.getElementById('stock').value==""||
                        document.getElementById('precio').value==""||
                        document.getElementById('precio').value<=0||
                        document.getElementById('precio').value<=0||
                        document.getElementById('marca').value.length==0||
                        document.getElementById('modelo').value.length==0|| 
                        document.getElementById('stock').value.length==0||
                        document.getElementById('precio').value.length==0||
                        parseFloat(document.getElementById('precio').value==false)) {
                        return ["camposvacios"];
                    }else{
                        return [
                            this.marca = document.getElementById('marca').value,
                            this.modelo = document.getElementById('modelo').value,
                            parseInt(this.stock = document.getElementById('stock').value),
                            parseFloat(this.precio = document.getElementById('precio').value.replace(",",".")),
                        ]
                    }
                },
       
              });
              //console.log(formValues);
              //Con el siguiente fragmento de codigo corrijo el error de al darle a añadir
              //nuevo producto, y cancelar, siempre salia una alerta de campos vacios.
              if(formValues==undefined){
                    //console.log("NO HACER NADA")
              }else if(formValues=="camposvacios"||isNaN(formValues[3])||isNaN(formValues[2])){
                  //console.log("CON CAMPOS VACIOS")
                  Swal.fire({
                    icon: 'error',
                    //type:'info',
                    title: 'Datos incompletos o erroneos',
                })
              }else{
                //console.log("SIN CAMPOS VACIOS")

                 //Funcion para dar de alta una nueva consola:
                 this.altaConsola();
                 //Mensaje que aparece cuando se agrega exitosamente un producto
                 const Toast = Swal.mixin({
                      toast:true,
                      position: 'center',
                      showConfirmButton:false,
                      timer:3000,
                 });
                 Swal.fire(
                  '¡Consola Añadida!',
                  'El registro ha sido creado.',
                  'success'
              )
            }
      
        },
        botonEditar: async function(id,marca,modelo,stock,precio){
            await Swal.fire({
                title: 'Editar Consola',
                html:
                    `<br><div class="row"><label class="col-sm-3 col-form-label">Marca</label><div class="col-sm-7"><input id="marca" value="`+marca+`" type="text" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Modelo</label><div class="col-sm-7"><input id="modelo" value="`+modelo+`" type="text" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Stock</label><div class="col-sm-7"><input id="stock" value="`+stock+`" type="number" min="0" class="form-control"></div></div><br>
                     <div class="row"><label class="col-sm-3 col-form-label">Precio</label><div class="col-sm-7"><input id="precio" value="`+precio+`" type="number" min="0" class="form-control"></div></div><br>
                    `,
                    focusConfirm: false,
                    showCancelButton: true,
            }).then((result) => {
                if(result.value){
                    marca = document.getElementById('marca').value,
                    modelo = document.getElementById('modelo').value,
                    stock = document.getElementById('stock').value,
                    precio = document.getElementById('precio').value,
                    this.editarConsola(id,marca,modelo,stock,precio);
                    Swal.fire(
                        '¡Consola Actualizada!',
                        'El registro ha sido actualizado.',
                        'success'
                    )
                }
            });
        },
        botonEliminar: function(id){
            Swal.fire({
                title: '¿Deseas borrar la consola con id: '+id+' ?',
                //type: 'warning',
                showCancelButton: true,
                confirmButtonText:'Borrar',
                cancelButtonText:'Cancelar',
                confirmButtonColor:'#7CD825',
                cancelButtonColor:'#D85625',
            }).then((result)=>{
                if(result.value){
                    this.borrarConsola(id);
                    Swal.fire(
                        '¡Consola Eliminada!',
                        'El registro ha sido borrado',
                        'success'
                    )
                }
            })
        },
        botonBuscarMarca:function(){
            $(".btnMostrar").show();
            var articulosEncontrados;



            marcaBuscar = $("#marcaBuscar").val();
            //console.log(marcaBuscar);

            //Aqui llamamos a Axios (Funciona parecido a las consultas con ajax y jquery)
            axios.post(url,{opcion:5,marcaBuscar:marcaBuscar}).then(response =>{
                this.consolas = response.data;
                articulosEncontrados = response.data.length; 
                
                //Para calcular la ganancia de cada consola segun el stock y el precio:
                //console.log(response.data[0].precio*response.data[0].stock)
                for(consola of this.consolas){
                    //console.log(consola.precio)
                    consola.ganancia = (parseFloat(consola.precio) * parseFloat(consola.stock)).toFixed(2);
                    
                }//Fin calcular ganancia segun stock y precio

                if (articulosEncontrados==0){
                    
                    Swal.fire({
                        icon:'error',
                        title:'No se han encontrado articulos.'
                    })


                }
    

            });            
           
            
        },
        botonMostrarTodo:function(){
            $(".btnMostrar").hide();
            this.listarConsolas();
        },

        //PROCEDIMIENTOS
        listarConsolas: function(){
            //Aqui llamamos a Axios (Funciona parecido a las consultas con ajax y jquery)
            axios.post(url,{opcion:4}).then(response =>{
                this.consolas = response.data;
                //console.log(response.data);    

                //Para calcular la ganancia de cada consola segun el stock y el precio:
                //console.log(response.data[0].precio*response.data[0].stock)
                for(consola of this.consolas){
                    //console.log(consola.precio)
                    consola.ganancia = (parseFloat(consola.precio) * parseFloat(consola.stock)).toFixed(2);
                    
                }//Fin calcular ganancia segun stock y precio
            })
        },
        //PROCEDIMIENTO PARA CREAR CONSOLA
        altaConsola: function(){
            axios.post(url,{opcion:1,marca:this.marca,modelo:this.modelo,stock:this.stock,precio:this.precio}).then(response=>{
                //console.log(response);
                this.listarConsolas();
            
            });
            this.marca="";
            this.modelo="";
            this.stock=0;
            this.precio=0;
        },
        //PROCEDIMIENTO PARA EDITAR CONSOLA
        editarConsola:function(id,marca,modelo,stock,precio){
            axios.post(url,{opcion:2,id:id,marca:marca,modelo:modelo,stock:stock,precio:precio}).then(response=>{
                this.listarConsolas();
                //console.log(response);
            })
        },
        //PROCEDIMIENTO PARA BORRAR CONSOLA
        borrarConsola: function(id){
            axios.post(url,{opcion:3,id:id}).then(response =>{
                this.listarConsolas();
            });
        },

       
    },
    created:function(){
        this.cargarPagina();
        this.listarConsolas();
        
    
       
        
    },
    computed:{
        //Hacer calculos

        totalStock(){
            this.total=0;
            for(consola of this.consolas){
                this.total = this.total + parseInt(consola.stock);
            }
            return this.total;
        },
        totalGanancias(){//Para calcular las ganancias de cada producto
            this.ganancias=0;
            var TOTAL = 0;
            for(consola of this.consolas){
                TOTAL = parseFloat(consola.ganancia) + TOTAL;
            }
            //console.log(TOTAL);
            return TOTAL.toFixed(2);
        }
    }

});

//ALTER TABLE 'consolas' AUTO_INCREMENT=1 (Para reiniciar los id en la base de datos)
