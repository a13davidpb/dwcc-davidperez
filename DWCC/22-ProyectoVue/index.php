<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="viewpoint" content="width=device-width, initial-scale=1, syrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aplicacion</title>

    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!--FontAwesomCSS-->
    <script src="https://kit.fontawesome.com/bd632f581b.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sh....-->
    <!--SWEETALERT2-->
    <link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="estilos.css"/>
    
</head>
<body>



    
    <!--Estructura de la parte grafica del proyecto-->
    <div id="appConsolas">
        <nav style="height:80px;padding-top:7px" class="navbar sticky-top navbar-light bg-dark justify-content-between">
        <h3 id="textoCabecera" class="text-light">VUE CRUD <span style="font-size:30px">David Pérez</span></h3>
        <div class="form-inline">
            <input id="marcaBuscar" class="form-control mr-sm-2" type="text" placeholder="Busqueda por Marca" aria-label="Search">
            <button class="btn btn-outline-success bg-success text-light my-2 my-sm-0" @click="botonBuscarMarca">Buscar</button>
            &nbsp;
            <button class="btn btn-outline-primary bg-primary text-light my-2 my-sm-0 btnMostrar" @click="botonMostrarTodo">Mostrar Todo</button>
        </div>
        </nav>
        <br><br>




        <div class="container">
            <div class="row">
                <div class="col">
                    <!--Boton para agregar un nuevo consola-->
                     <h3 class="letra">Añadir Productos <button @click="botonNuevo" class="btn btn-primary" title="Nuevo"><i class="fas fa-plus fa-1x"></i></button></h3>
                </div>
                <div class="col text-right">
                    <h5><span class="letra">Stock total:</span> <span class="badge badge-dark">{{totalStock}}</span></h5>
                    <h5><span class="letra">Ganancias estimadas:</span> <span class="badge badge-success">{{totalGanancias}} €</span></h5>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                            <tr class="bg-dark text-light">
                                <th>ID</th>
                                <th style="text-align:center">Marca</th>
                                <th style="text-align:center">Modelo</th>            
                                <th style="text-align:center">Stock</th>            
                                <th>Precio/Ud</th>
                                <th>Ganancias</th>
                                <th style="text-align:center">Acción</th>                                        
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(consola,indice) of consolas">
                                <td>{{consola.id}}</td>
                                <td style="text-align:center" class="letra">{{consola.marca}}</td>
                                <td style="text-align:center">{{consola.modelo}}</td>

                                <td style="text-align:center">
                                    <!--<div class="col-md-8">
                                        <input type="number" v-model.number="consola.stock" class="form-control text-right" disabled>
                                    </div>-->
                                    {{consola.stock}}
                                </td>
                                <td>{{consola.precio}}€</td>
                                <td>{{consola.ganancia}}€</td>
                                <td style="text-align:center">
                                    <div class="btn-group" role="group">
                                        <!--Boton de editar-->
                                        <button class="btn btn-secondary" title="Editar" @click="botonEditar(consola.id,consola.marca,consola.modelo,consola.stock,consola.precio)"><i class="fas fa-edit"></i></button>
                                        <!--Boton de eliminar-->
                                        <button class="btn btn-danger" title="Eliminar" @click="botonEliminar(consola.id)"><i class="fa fa-trash-alt"></i></button>
                                    </div>
                                </td>
                            </tr>




                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!--Llamo a las librerias---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="jquery/jquery.js"></script>
    <!--Popper-->
    <!--<script src="popper/popper.min.js"></script>ME DA ERROR EN FIREFOX-->
    <!--<script src="https://unpkg.com/@popperjs/core@2"></script>ME DA ERROR EN FIREFOX-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
   
    <!--BOOTSTRAP-->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!--CDN de Bootstrap por si acaso-->
    <!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    -->

    <!-- CDN de Vue -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <!-- CDN de Axios -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <!--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>--> 
     
    <!--SWEETALERT2-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>-->
    <script src="plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
   
    <script src="javascript.js"></script>

</body>
</html>