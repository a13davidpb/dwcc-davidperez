//PARA QUE FUNCIONE EL COMANDO "nodemon server.js"
//hay que tener instalado los siguientes modulos:
//npm install -save socket.io fs nodemon express

//Puerto de trabajo
const puerto = 8080;
//Cargamos el modulo express
const moduloExpress = require('express');

//Creamos una nueva aplicacion sobre app llamado moduloExpress()
const app = moduloExpress();

//Creamos el objeto "servidor"
const servidor = require('http').createServer(app);

//Creamos el objeto io para los websockets
const io = require('socket.io').listen(servidor);
//Recibir peticiones por puerto
servidor.listen(puerto);
//Mostrar en consola el puerto en el que escucha
console.log("Servidor websockets en puerto: "+puerto);

//Gestionamos rutas peticiones
app.get('/',function(req,res){
    res.end("Servidor websockets para DWCC IES San Clemente");
});


/////////////////////////////////////////////////////////////////////////////////////
////////////  PROGRAMACION DE LOS EVENTOS QUE GESTIONA EL SERVIDOR  /////////////////
//  https://github.com/LearnBoost/socket.io/wiki/Exposed-events
/////////////////////////////////////////////////////////////////////////////////////

io.sockets.on('connection', function(socket)
{
    
    // Cada vez que se conecta un cliente mostramos un mensaje en la consola de Node.
    var ID=socket.id;
    console.log("ID CONEXION -> "+socket.id);

    // Crear 2 canales: avisarconserje e incidenciasresueltas

    socket.on("avisarconserje", function(datos){       
        //Datos que llegan al servidor:
        console.log("ID usuario: "+datos.idUsuario)
        console.log("Mensaje de: "+datos.nombreUsuario);
        console.log("Aula: "+datos.aulaUsuario);
        console.log("Mensaje del usuario: "+datos.mensajeUsuario);

        //DATOS QUE EMITE EL SERVIDOR:
        io.sockets.emit('incidenciasresueltas',datos);
        //io.sockets.emit('avisarconserje',datos);
    });
    

	socket.on("incidenciasresueltas",function(datos){

        //Datos que emite el servidor:
        //io.sockets.emit('incidenciasresueltas',datos);
        io.sockets.emit('avisarconserje',datos);
    });
	
	
	//Al desconectar un usuario que avise por consola
      socket.on("disconnect",function(socket){
        console.log("Usuario desconectado: "+ID);
    });


});