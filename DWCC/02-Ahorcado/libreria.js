
//Generar una palabra aleatoria al recargar la página:
const palabras = arrayPalabras();      
//const palabras = arrayPrueba;

//Crear un array solo con las palabras mayores de 5 letras
var arrayUtilizar = [];
for(var i = 0; i<palabras.length;i++){
    if(palabras[i].length>=5){

        palabras[i]=quitaAcentos(palabras[i]);

        arrayUtilizar.push(palabras[i].toLowerCase());
    }

}//console.log(arrayUtilizar);

//LA PALABRA ALEATORIA:
var numAle = Math.floor((Math.random()*arrayUtilizar.length)+0);
var palabraAleatoria = arrayUtilizar[numAle];
//console.log("La palabra aleatoria es: "+palabraAleatoria+" (Con mayusculas pasadas a minusculas y sin acentos)");





//Funcion que quita acentos a una palabra.
function quitaAcentos(string){
    var r=string.toLowerCase();
    r = r.replace(new RegExp(/\s/g),"");
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/ñ/g),"n");                
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");            
    return r;
}



console.log(palabraAleatoria);
//Crear un array con cada letra de la palabra:
var arrayPalabra = palabraAleatoria.toUpperCase().split("");
//console.log("El array con la palabra: "+arrayPalabra);

//Crear un array de guiones:
var arrayGuiones = [];
for(var i = 0;i<arrayPalabra.length;i++){
    arrayGuiones.push("-");
}
//console.log("El array con los guiones: "+arrayGuiones);



//dibujaTeclas("teclado");

function dibujaTeclas(){
    var id1 = "teclado1";
    var id2 = "teclado2";
    var id3 = "teclado3";

    const letras1 = ["Q","W","E","R","T","Y","U","I","O","P"];
    const letras2 =["A","S","D","F","G","H","J","K","L","Ñ"];
    const letras3 =["Z","X","C","V","B","N","M"];
    
    
    let div1;
    //Cada una de las letras será un div.
        
        //Creamos un div vacio:
        div1 =  document.createElement('div');

        //Ese div va a tener dos estilos:
        div1.classList.add("letter","key");
        
        //Obtenemos el id del div.
        divID1 = document.getElementById(id1);
        

        //Empezamos a crearlas:
        for(let i =0;i<letras1.length;i++){
            div1 = div1.cloneNode(false);//Si no queremos que lleve nada dentro

            


            div1.innerHTML=letras1[i];//Le añadimos cada letra
            
            //Le añadimos lo que queremos que pase cuando hacemos click en el:
            div1.addEventListener('click',enJuego); //Se llama a la funcion en juego
            
            //Falta añadir todo al div con id "botones"
            divID1.appendChild(div1);
        }
    let div2;
    //Cada una de las letras será un div.
        
        //Creamos un div vacio:
        div2 =  document.createElement('div');

        //Ese div va a tener dos estilos:
        div2.classList.add("letter","key");

        //Obtenemos el id del div.
        divID2 = document.getElementById(id2);
        

        //Empezamos a crearlas:
        for(let i =0;i<letras2.length;i++){
            div2 = div2.cloneNode(false);//Si no queremos que lleve nada dentro

            


            div2.innerHTML=letras2[i];//Le añadimos cada letra
            
            //Le añadimos lo que queremos que pase cuando hacemos click en el:
            div2.addEventListener('click',enJuego); //Se llama a la funcion en juego
            
            //Falta añadir todo al div con id "botones"
            divID2.appendChild(div2);

            
            }

    let div3;
    //Cada una de las letras será un div.
        
        //Creamos un div vacio:
        div3 =  document.createElement('div');

        //Ese div va a tener dos estilos:
        div3.classList.add("letter","key");

        //Obtenemos el id del div.
        divID3 = document.getElementById(id3);
        

        //Empezamos a crearlas:
        for(let i =0;i<letras3.length;i++){
            div3 = div3.cloneNode(false);//Si no queremos que lleve nada dentro

            


            div3.innerHTML=letras3[i];//Le añadimos cada letra
            
            //Le añadimos lo que queremos que pase cuando hacemos click en el:
            div3.addEventListener('click',enJuego); //Se llama a la funcion en juego
            
            //Falta añadir todo al div con id "botones"
            divID3.appendChild(div3);

            
            }




}
var divIdPalabra;        
//declaro el numero de intentos:
var intIntentos = document.getElementById("intentos").innerHTML;
var error = parseInt(intIntentos);
var victoria = false;

function enJuego(){
    
    
    this.classList.add('hidden');
   // console.log(this.innerHTML);

    let sL = this.innerHTML;
    let existe = false;

    for(let i=0;i<arrayPalabra.length;i++){
        if(sL == arrayPalabra[i]){
            arrayGuiones[i] = sL;
            let divIdPalabra=document.getElementById('idPalabra');
            divIdPalabra.innerHTML = arrayGuiones.join('');
            existe = true;
            error=error;
         }
    }

    if(existe==false){
        error++;
    }
    //console.log("error:"+error);
    //console.log(arrayGuiones);
    

    //Aumento un intento
    intIntentos++;
    document.getElementById("intentos").innerHTML=intIntentos;

    if(error == 1 && existe==false){
        //insertar Imagen¿?¿
        let img1 = document.getElementById("img1");
        img1.classList.remove("img1");
        img1.classList.add("visible");
    }

    if(error == 2 && existe==false){
        //insertar Imagen¿?¿
        let img1 = document.getElementById("img1");
        img1.src="imagenes/2.png";
        img1.classList.remove("img1");
        img1.classList.add("visible");        
    }


    if(error == 3 && existe==false){
        //insertar Imagen¿?¿
        let img1 = document.getElementById("img1");
        img1.src="imagenes/3.png";
        img1.classList.remove("img1");
        img1.classList.add("visible");        
    }


    if(error == 4 && existe==false){
        //insertar Imagen¿?¿
        let img1 = document.getElementById("img1");
        img1.src="imagenes/4.png";
        img1.classList.remove("img1");
        img1.classList.add("visible");        
    }

    if(error == 5 && existe==false){
        //insertar Imagen¿?¿
        let img1 = document.getElementById("img1");
        img1.src="imagenes/8.png";
        img1.classList.remove("img1");
        img1.classList.add("visible");
        
        //Ocultamos la frase "numero de intentos"
        nIntentos=document.getElementById("nIntentos");
        nIntentos.classList.add("nIntentosOcultar");
        
        //Muestro boton reiniciar
        document.getElementById("restart").classList.remove("ocultoRestart");
        document.getElementById("restart").classList.add("visibleRestart", "intentosRestart");


        document.getElementById("intentos").innerHTML="Has perdido";
        document.getElementById("idPalabra").innerHTML="Palabra: "+palabraAleatoria.toUpperCase();

      
        
    }
    //SI SE PIERDE, SE RECARGA LA PAGINA CON EL SIGUIENTE CLICK
      if(document.getElementById("intentos").innerHTML == "Has perdido"){
        let aDivLetrasTeclado1 =document.getElementById("teclado1").childNodes;
        let aDivLetrasTeclado2 =document.getElementById("teclado2").childNodes;
        let aDivLetrasTeclado3 =document.getElementById("teclado3").childNodes;

        for(let i=0; i<aDivLetrasTeclado1.length;i++){
            aDivLetrasTeclado1[i].addEventListener('click',reload); 
            aDivLetrasTeclado1[i].classList.remove('hidden');          
        }
        for(let i=0; i<aDivLetrasTeclado2.length;i++){
            aDivLetrasTeclado2[i].addEventListener('click',reload);           
            aDivLetrasTeclado2[i].classList.remove('hidden');
        }
        for(let i=0; i<aDivLetrasTeclado3.length;i++){
            aDivLetrasTeclado3[i].addEventListener('click',reload);           
            aDivLetrasTeclado3[i].classList.remove('hidden');
        }

    }

    
    //Compruebo si se acabo la partida: (SI SE GANA LA PARTIDA)
    if(arrayGuiones.includes('-')==false){
        //console.log("PARTIDA TERMINADA")
        victoria=true;
        document.getElementById("intentos").innerHTML="Has ganado";        


        //Ocultamos la frase "numero de intentos"
        nIntentos=document.getElementById("nIntentos");
        nIntentos.classList.add("nIntentosOcultar");


        document.getElementById("restart").classList.remove("ocultoRestart");
        document.getElementById("restart").classList.add("visibleRestart", "intentosRestart");


        let aDivLetrasTeclado1 =document.getElementById("teclado1").childNodes;
        let aDivLetrasTeclado2 =document.getElementById("teclado2").childNodes;
        let aDivLetrasTeclado3 =document.getElementById("teclado3").childNodes;


        for(let i=0; i<aDivLetrasTeclado1.length;i++){
            aDivLetrasTeclado1[i].addEventListener('click',reload);           
            aDivLetrasTeclado1[i].classList.remove('hidden');
            //No mostramos el dibujo:
            let img1 = document.getElementById("img1");
            img1.classList.remove("visible");
            img1.classList.add("img1");
        
        }
        for(let i=0; i<aDivLetrasTeclado2.length;i++){
            aDivLetrasTeclado2[i].addEventListener('click',reload);           
            aDivLetrasTeclado2[i].classList.remove('hidden');
            //No mostramos el dibujo:
            let img1 = document.getElementById("img1");
            img1.classList.remove("visible");
            img1.classList.add("img1");
        
        }
        for(let i=0; i<aDivLetrasTeclado3.length;i++){
            aDivLetrasTeclado3[i].addEventListener('click',reload);           
            aDivLetrasTeclado3[i].classList.remove('hidden');
            //No mostramos el dibujo:
            let img1 = document.getElementById("img1");
            img1.classList.remove("visible");
            img1.classList.add("img1");
        
        }



        //Mostramos las letras
        for(let i=0; i<aDivLetrasTeclado1.length;i++){
            aDivLetrasTeclado1[i].addEventListener('click',reload);

            //Mostramos las letras
            aDivLetrasTeclado1[i].classList.remove('hidden');
        }



        
    }



    existe=false;



 //   console.log(intIntentos);
}

function reload(){
    location.reload();    
}

function inicio(){
    let tamMinPalabra = 6;
    //La palbra en juego la pasamos a array
    aPalabraOculta = palabraAleatoria.split('');
   // console.log(aPalabraOculta);

    //Miramos el tamaño de la palabra de juego
    iTamPalabraOculta = aPalabraOculta.length;

    //Creamos un array de guiones del tamaño de la palabra de juego
    aPalabraGuiones = '-'.repeat(iTamPalabraOculta).split('');
    let divIdPalabra = document.getElementById('idPalabra');
    divIdPalabra.innerHTML =  aPalabraGuiones.join('');


}