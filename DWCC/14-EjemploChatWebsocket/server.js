//PARA QUE FUNCIONE EL COMANDO "nodemon server.js"
//hay que tener instalado los siguientes modulos:
//npm install -save socket.io fs nodemon express

//Puerto de trabajo
const puerto = 8080;
//Cargamos el modulo express
const moduloExpress = require('express');

//Creamos una nueva aplicacion sobre app llamado moduloExpress()
const app = moduloExpress();

//Creamos el objeto "servidor"
const servidor = require('http').createServer(app);

//Creamos el objeto io para los websockets
const io = require('socket.io').listen(servidor);
//Recibir peticiones por puerto
servidor.listen(puerto);
//Mostrar en consola el puerto en el que escucha
console.log("Servidor websockets en puerto: "+puerto);

//Gestionamos rutas peticiones
app.get('/',function(req,res){
    res.end("Servidor websockets para DWCC IES San Clemente");
});

//Programamos las respuestas a los eventos de io
io.sockets.on("connection",function(socket){
    var ID=socket.id;
    console.log("Nuevo cliente con id: "+socket.id);
    //Cuando hay un mensaje//SE PUEDE QUITAR DESDE AQUI
    //socket.on("message",function(datos){
    //    io.sockets.emit('message',datos);
    //    console.log(datos.texto);
    //});//SE PUEDE QUITAR HASTA AQUI (PORQUE ESTAMOS PASANDO POR AVISOS)

    //Al desconectar un usuario que avise por consola
    socket.on("disconnect",function(socket){
        console.log("Usuario desconectado: "+ID);
    });

    //Creamos un canal nuevo llamado "avisos"
    socket.on("avisos",function(datos){
        io.sockets.emit('avisos',datos);
        console.log("Mensaje de: "+datos.usuario);
        console.log(datos.texto);
        
    })
});
