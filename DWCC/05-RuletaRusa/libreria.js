//Esta variable controlará que estés vivo siempre.
var vivo = true; //Será falsa cuando mueras.



const botonDisparar = document.getElementById("botonDisparar");
const botonVolverGirar = document.getElementById("volverGirar");
const contadorVeces = document.getElementById("contador");
const mensajeMuerto = document.getElementById("mensajeMuerto");
const botonReiniciar = document.getElementById("botonReiniciar");
const disparo = document.getElementById("disparo");



botonDisparar.addEventListener('click',disparar,false);


tambor = document.getElementById("tambor");

var contador=0;



function disparar(){  



        if(vivo==true){
            contadorVeces.classList.value="contenedorContador visible";
            //Genero un numero aleatorio entre 1 y 6
            let numAle = Math.floor((Math.random() * (1-7))+7);
            tambor.classList.value="rotating"+numAle+" tambor"; 

            botonDisparar.classList.value="button oculto";
            botonVolverGirar.classList.value="button";

            if(tambor.classList.value=="rotating1 tambor"){ 
                //MUERES:(TAMBIEN HAY QUE ESPECIFICAR QUE PASA SI MUERES EN EL PRIMER DISPARO AQUI):
                botonVolverGirar.classList.value="button oculto";
                contadorVeces.classList.value="contenedorContador oculto";
                vivo=false;

                if(parseInt(contador)>1){
                    //Cambio el fondo:                
                    document.body.style.background = "#f3f3f3 url('./imagenes/backgroundbloody.jpg')"; 
                    //Muestro el gif del disparo:                
                    disparo.classList.value="visible";
                    //Oculto el gift para dar un efecto de disparo limpio:
                    setTimeout(function(){ disparo.classList.value="oculto"; },1000);


                    mensajeMuerto.innerHTML="HAS MUERTO <br><span class='mensaje'>Arriesgando tu vida "+contador+" veces.</span>"
                    mensajeMuerto.classList.value="contenedorMuerto visible bordeado";
                }else if(parseInt(contador)==1){
                    //Cambio el fondo:                
                    document.body.style.background = "#f3f3f3 url('./imagenes/backgroundbloody.jpg')"; 
                    //Muestro el gif del disparo:                
                    disparo.classList.value="visible";
                    //Oculto el gift para dar un efecto de disparo limpio:
                    setTimeout(function(){ disparo.classList.value="oculto"; },1000);

                    mensajeMuerto.innerHTML="HAS MUERTO <br><span class='mensaje'>Arriesgando tu vida "+contador+" vez.</span>"
                    mensajeMuerto.classList.value="contenedorMuerto visible bordeado";
                }else if(parseInt(contador)==0){
                    //Cambio el fondo:                
                    document.body.style.background = "#f3f3f3 url('./imagenes/backgroundbloody.jpg')"; 
                    //Muestro el gif del disparo:                
                    disparo.classList.value="visible";
                    //Oculto el gift para dar un efecto de disparo limpio:
                    setTimeout(function(){ disparo.classList.value="oculto"; },1000);

                    mensajeMuerto.innerHTML="HAS MUERTO <br><span class='mensaje'>Has muerto a la primera.</span>"
                    mensajeMuerto.classList.value="contenedorMuerto visible bordeado";
                }

                botonReiniciar.addEventListener('click',reload,false);
                botonReiniciar.classList.value="button visible bordeado";

                
                console.log("MUERTO");
            }else{
                //SIGUES VIVO - PUEDES SEGUIR JUGANDO:
                botonVolverGirar.addEventListener('click',volverAGirar,false);
                contador++;
                contadorVeces.innerHTML="Veces<br>que<br>Sobreviviste<br>"+contador;
                
                console.log("Has sobrevivido: "+contador);
            }
        }

}

function volverAGirar(){
        botonVolverGirar.classList.value="button oculto";
        botonDisparar.classList.value="button";
        tambor.classList.value="rotating tambor";
}

function reload(){
    location.reload();
}

