<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8"/>
	<title>Edición de Registros con jQuery</title>
	<style type="text/css">
		tr.centrado {
			text-align: center;
		}
		.actual {
			font-size: 30px;
			padding-left: 10px;
			padding-right: 10px;
		}
		.bien{
			background:lightgreen;
		}
		.mal{
			background:orange;
		}
		
	</style>
</head>
<body>

	<?php
	require("conexion.php");
	$registrosPorPagina = 10;
	$paginaActual = 0;

	$stmt=$pdo->query("select count(*)as total from paises2");
	$fila=$stmt->fetch();

	$registrosTotales = $fila['total'];
	$paginasTotales = ceil($registrosTotales / $registrosPorPagina);

///////////// Cálculo de la página actual.  ///////////////////
	if (isset($_GET['pagina'])) {
		$paginaActual = (int) $_GET['pagina'];
	}

		// El número de la página actual no puede ser menor a 0
	if ($paginaActual < 1) {
		$paginaActual = 1;
		} else if ($paginaActual > $paginasTotales) { // tampoco mayor la cantidad de páginas totales
			$paginaActual = $paginasTotales;
		}


///////////// Generación de la consulta.  ///////////////////

		$registroInicial = ($paginaActual - 1) * $registrosPorPagina;
		$stmt=$pdo->prepare("select * from paises2 order by nombrepais ASC limit :inicio,:fin");
		$stmt->bindValue(':inicio',$registroInicial,PDO::PARAM_INT);
		$stmt->bindValue(':fin',$registrosPorPagina,PDO::PARAM_INT);
		$stmt->execute();
		/*
		$sql = "select * from paises order by nombrepais ASC limit $registroInicial,$registrosPorPagina";
		$resultado = $mysqli->query($sql);
		*/
		echo "<h2 style=\"display:inline\">LISTADO DE PAISES</H2> <img src='img/add.png'/>";
		echo "<form id='miformulario'><table border=1 id=milistado>";
		echo "<tr><th>País</th><th>Región</th><th>Área</th><th>Población</th><th>GDP</th><th colspan=2>Edición</th></tr>";

		while ($fila = $stmt->fetch()) {
			echo "<tr class='centrado'><td>" . $fila['nombrepais'] . "</td><td>" . $fila['region'] . "</td><td>" . $fila['area'] . "</td><td>" . $fila['poblacion'] . "</td><td>" . $fila['gdp'] . "</td><td> <img src='img/edit.png'/></td><td><img src='img/close.png'/></td></tr>";
		}

		echo "</table></form>";

///////////// Mostramos la paginación  ///////////////////
		$paginaSiguiente = $paginaActual + 1;
		$paginaAnterior = $paginaActual - 1;
		if ($paginaActual != 1) {
			echo "<a href='?pagina=1'/><img src='img/sq_br_first_icon24.png'/></a>";
			echo "<a href='?pagina=$paginaAnterior'/><img src='img/sq_br_prev_icon24.png'/></a>";
		}
		echo '<span class=actual>' . $paginaActual . '</span>';

		if ($paginaActual != $paginasTotales) {
			echo "<a href='?pagina=$paginaSiguiente'/><img src='img/sq_br_next_icon24.png'/></a>";
			echo "<a href='?pagina=$paginasTotales'/><img src='img/sq_br_last_icon24.png'/></a>";
		}
		?>


		<!--EMPEZAMOS AQUI-->
		<script src="./js/jquery-3.4.1.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){

					//Para que aparezca el cursor "pointer" en las imagenes 
					$("img[src*='edit'").mouseover().css('cursor','pointer');
					$("img[src*='close'").mouseover().css('cursor','pointer');
					$("img[src*='add'").mouseover().css('cursor','pointer');


					//Desactivar cache navegador
					$.ajaxSetup({cache:false});

					//Programamos evento EDICION
					//El evento click sobre cualquiera de las imagenes que tengan el src 'edit' dentro de la tabla #milistado
					$("#milistado").on("click","img[src*='edit']",function(){
							
							//Si se quiere editar otro registo distinto al que está editando ahora
							//Evitar dos ediciones a la vez!
							if($("input.edicion").length!=0){
								padreTR.html(contenidoOriginalFila);
							};
						
						
						if(confirm('¿Desea editar este registro?')){	
							//NECESITAMOS LLEGAR AL "tr"
							padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
							//Obtenemos el nombre del pais de la fila
							paisOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del pais							
							//Contenido original de la fila:(Se necesitara en un futuro cuando se seleccione otra fila)
							contenidoOriginalFila=padreTR.html();
						

							//Recorremos los td y convertimos el texto en cajas de texto
							padreTR.find("td").slice(0,5).each(function(){
								//Creamos cajas de texto en cada campo de la fila
								$(this).html("<input type='text' class='edicion' value='"+$(this).text()+"'/>");//Con esto podremos editar la fila 
							});

							//Al pulsar Enter en alguna de las cajas de texto... realiza la consulta de edicion
							$("input").keyup(function(evento){
								//el numero 13 es el codigo del "INTRO"
								if(evento.which==13){//Si pulsamos ENTER hacemos la peticion ajax...
									var datosEnviar={
										nombrepais:$("input:eq(0)").val(),
										region:$("input:eq(1)").val(),
										area:$("input:eq(2)").val(),
										poblacion:$("input:eq(3)").val(),
										gdp:$("input:eq(4)").val(),
										paisoriginal:paisOriginal,
									};
									//console.log(datosEnviar);

									//Hacemos la consulta a la base de datos
									$.post("peticiones.php?op=1",datosEnviar,function(respuesta){
										console.log("Datos actualizados: "+respuesta);
										//Salir de la edicion y mostrar durante 1 segundo la fila editada en color verde:
										if(respuesta == "ok"){
											//Sacamos los input y dejamos las celdas
											padreTR.find("td").slice(0,5).each(function(){
												$(this).text($(this).find("input").val());
											});
											//Poner fondo VERDE durante un segundo
											padreTR.addClass('bien').delay(1000).queue(function(){
												$(this).removeClass('bien');
											});//Fin VERDE
										}	



									});//Fin POST edicion de datos


									

								};
							});//Fin pulsar enter edicion de datos

						};//Fin confirmacion de edicion	

					});//Fin click EDICION




						//ELIMINAR registros de la base de datos
						$("#milistado").on("click","img[src*='close']",function(){
							
							//Si se esta modificando una fila, volver a su estado original
							if($("input").length!=0){//Si hay algun input es que algo se estaba modificando
								padreTR.html(contenidoOriginalFila);
							}

							if(confirm("¿Desea borrar este registro?")){
								padreTR = $(this).parent().parent();
								//console.log(padreTR.children().first().text());
								//paisOriginal = padreTR.find("td:eq(0)").text();
								//console.log(paisOriginal);

								var paisEliminar = padreTR.find("td:eq(0)").text();

								var datosEnviar = {nombrepais:paisEliminar};
								$.post("peticiones.php?op=2",datosEnviar, function(respuesta){
									console.log(respuesta);
									if(respuesta=="ok"){
										padreTR.addClass("mal").delay(1000).queue(function (){
											//Elimino de la pagina la fila
											$(this).remove();
											//Recargo pagina
											location.reload();
										});
									};

								});

							};

						});//FIN ELIMINAR REGISTROS


						//AGREGAR registros a la base de datos
						$("img[src*='add']").click(function(){
							//Comprobamos si hay algun registro en edicion
							if($("input.edicion").length!=0){
								padreTR.html(contenidoOriginalFila);
							}
							//Ocultar la imagen añadir filas para no pulsarla dos veces seguidas
							$(this).hide();
							//Crear una fila nueva con campos de forumlario (Campos input)
							$("<tr><td><input class='altas' type='text'/></td><td><input class='altas' type='text'/></td><td><input class='altas' type='text'/></td><td><input class='altas' type='text'/></td><td><input class='altas' type='text'/></td><td>&nbsp;</td><td>&nbsp;</td></tr>").insertAfter("tr:eq(0)");

							//Pulsar ENTER y realizar una pequeña validacion de este formulario:
								$("input").keyup(function(evento){
								if(evento.which ==13 && $("input:eq(0)").val()!=""){
									//Hacemos la peticion AJAX de añadir nuevo elemento a la BD
									//nombrepais, region, area, poblacion, gdp

									var datosEnviar = { 
										nombrepais:$("input:eq(0)").val(),
										region:$("input:eq(1)").val(),
										area:$("input:eq(2)").val(),
										poblacion:$("input:eq(3)").val(),
										gdp:$("input:eq(4)").val(),
									};

									console.log(datosEnviar);

								}
							})


						});



				
			});//Fin document#ready



		</script>

</body>
</html>