<?php
require("conexion.php");

switch ($_GET['op']) {
	case 1:  // Edición de datos.
	try {
		$stmt=$pdo->prepare("update paises2 set nombrepais=:nombrepais,  region=:region, area=:area, poblacion=:poblacion, gdp=:gdp, bandera=:bandera where nombrepais=:nombrepaisoriginal");
		$stmt->bindValue(':nombrepais',$_POST['nombrepais']);
		$stmt->bindValue(':region',$_POST['region']);
		$stmt->bindValue(':area',$_POST['area']);
		$stmt->bindValue(':poblacion',$_POST['poblacion']);
		$stmt->bindValue(':gdp',$_POST['gdp']);
		$stmt->bindValue(':nombrepaisoriginal',$_POST['paisoriginal']);

		
		//Para el archivo   'bandera'
		$fileTmpPath = $_FILES['bandera']['tmp_name'];
    	$fileName = $_FILES['bandera']['name'];
    	$fileSize = $_FILES['bandera']['size'];
    	$fileType = $_FILES['bandera']['type'];
    	$fileNameCmps = explode(".", $fileName);
    	$fileExtension = strtolower(end($fileNameCmps));
    	$newFileName = md5(time() . $fileName) . '.' . $fileExtension;

    	$extensionspermitidas = array('jpg', 'gif', 'png');
		if (in_array($fileExtension, $extensionspermitidas)) 
		{
			//Guardar archivo en el directorio
        	$uploadFileDir = './banderas/';
        	$dest_path = $uploadFileDir . $newFileName;
			move_uploaded_file($fileTmpPath, $dest_path);
			//Guardar en la BD
			$stmt->bindValue(':bandera', $dest_path);
		}
		else
		{
			echo 'Erro subindo o arquivo';
			break;
		}
		

		$stmt->execute();
		echo 'ok';
	}
	catch(PDOException $e)
	{
		echo 'duplicado';
	}
	break;

	case 2: // Borrado de un país.
	
	try{
		$stmt=$pdo->prepare("delete from paises2 where nombrepais=:nombrepais");
		$stmt->bindValue(':nombrepais',$_POST['nombrepais']);
		$stmt->execute();
		echo 'ok';
	}
	catch(PDOException $e)
	{
		echo 'error';
	}
	break;

	case 3: // Insertamos un registro.
	try{
		$stmt=$pdo->prepare("insert into paises2(nombrepais,region,area,poblacion,gdp) values(?,?,?,?,?)");
		$stmt->bindValue(1,$_POST['nombrepais']);
		$stmt->bindValue(2,$_POST['region']);
		$stmt->bindValue(3,$_POST['area']);
		$stmt->bindValue(4,$_POST['poblacion']);
		$stmt->bindValue(5,$_POST['gdp']);
		$stmt->execute();
		echo 'ok';
	}
	catch(PDOException $e)
	{
		echo 'duplicado';
	}
	break;
}