<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8"/>
	<title>Edición de Registros con jQuery</title>
	<style type="text/css">
		tr.centrado {
			text-align: center;
		}
		.actual {
			font-size: 30px;
			padding-left: 10px;
			padding-right: 10px;
		}
		
	</style>
</head>
<body>

	<?php
	require("conexion.php");
	$registrosPorPagina = 10;
	$paginaActual = 0;

	$stmt=$pdo->query("select count(*)as total from paises2");
	$fila=$stmt->fetch();

	$registrosTotales = $fila['total'];
	$paginasTotales = ceil($registrosTotales / $registrosPorPagina);

///////////// Cálculo de la página actual.  ///////////////////
	if (isset($_GET['pagina'])) {
		$paginaActual = (int) $_GET['pagina'];
	}

		// El número de la página actual no puede ser menor a 0
	if ($paginaActual < 1) {
		$paginaActual = 1;
		} else if ($paginaActual > $paginasTotales) { // tampoco mayor la cantidad de páginas totales
			$paginaActual = $paginasTotales;
		}


///////////// Generación de la consulta.  ///////////////////

		$registroInicial = ($paginaActual - 1) * $registrosPorPagina;
		$stmt=$pdo->prepare("select * from paises2 order by nombrepais ASC limit :inicio,:fin");
		$stmt->bindValue(':inicio',$registroInicial,PDO::PARAM_INT);
		$stmt->bindValue(':fin',$registrosPorPagina,PDO::PARAM_INT);
		$stmt->execute();
		/*
		$sql = "select * from paises2 order by nombrepais ASC limit $registroInicial,$registrosPorPagina";
		$resultado = $mysqli->query($sql);
		*/
		echo "<h2 style=\"display:inline\">LISTADO DE paises2</H2> <img src='img/add.png'/>";
		echo "<form id='miformulario'><table border=1 id=milistado>";
		echo "<tr><th>País</th><th>Región</th><th>Área</th><th>Población</th><th>GDP</th><th>Bandera</th><th colspan=2>Edición</th></tr>";

		while ($fila = $stmt->fetch()) {
			echo "<tr class='centrado'><td>" . $fila['nombrepais'] . "</td><td>" . $fila['region'] . "</td><td>" . $fila['area'] . "</td><td>" . $fila['poblacion'] . "</td><td>" . $fila['gdp'] . "</td><td><img src='" . $fila['bandera'] . "' height='60px' /></td><td> <img src='img/edit.png'/></td><td><img src='img/close.png'/></td></tr>";
		}

		echo "</table></form>";

///////////// Mostramos la paginación  ///////////////////
		$paginaSiguiente = $paginaActual + 1;
		$paginaAnterior = $paginaActual - 1;
		if ($paginaActual != 1) {
			echo "<a href='?pagina=1'/><img src='img/sq_br_first_icon&24.png'/></a>";
			echo "<a href='?pagina=$paginaAnterior'/><img src='img/sq_br_prev_icon&24.png'/></a>";
		}
		echo '<span class=actual>' . $paginaActual . '</span>';

		if ($paginaActual != $paginasTotales) {
			echo "<a href='?pagina=$paginaSiguiente'/><img src='img/sq_br_next_icon&24.png'/></a>";
			echo "<a href='?pagina=$paginasTotales'/><img src='img/sq_br_last_icon&24.png'/></a>";
		}
		?>

<script src="./js/jquery.js"></script>

<script>
$(document).ready(function() {
// Desactivación de la caché en peticiones Ajax.
	$.ajaxSetup( {cache: false} );

	// Primero asignar los eventos de esta forma.
	// $("img[src*='edit']").click(function()
	// Luego ponerlo con .delegate para que valga para los nuevos registros añadidos.
	// En .on hay que poner como segundo parametro el selector específico.
	// Ya que el evento burbujea hasta llegar al padre.
	$("#milistado").on("click", "img[src*='edit']", function() {
		if (confirm("¿Desea editar este registro?")) {
		// Comprobamos si hay alguna otra fila en Edición.
			if ($("input#edicion").length != 0) {
				padre.html(contenidoOriginalFila);
			}

						// Si está la fila de Altas activada, la eliminamos y activamos el botón de nueva.
						if ($("input#altas").length != 0)
						{
							// Eliminamos la fila 1 de edición.
							$("tr:eq(1)").remove();

							//Mostramos el botón oculto.
							$("img[src*='add']").show();
						}

						padre = $(this).parent().parent();
						paisOriginal = padre.find("td:eq(0)").text();
						contenidoOriginalFila = padre.html();



						// Recorremos las casillas y las convertimos en campos de texto.
						padre.find("td").slice(0, 5).each(function()
						{
							$(this).html("<input id='edicion' type='text' value='" + $(this).text() + "'/>");
						});
						padre.find("td").eq(5).html("<input type='file' name='bandera' accept='image/*'/>");

						$("input").keyup(function(evento) {
							if (evento.which == 13)
							{
								let datosEnviar = new FormData();
								datosEnviar.append('nombrepais', $("input:eq(0)").val());
								datosEnviar.append('region', $("input:eq(1)").val());
								datosEnviar.append('area', $("input:eq(2)").val());
								datosEnviar.append('poblacion', $("input:eq(3)").val());
								datosEnviar.append('gdp', $("input:eq(4)").val());
								datosEnviar.append('bandera', $("input[type=file]")[0].files[0]);
								datosEnviar.append('paisoriginal', paisOriginal);

								//Para ver los pares clave : valor
								for(var par of datosEnviar.entries()) {
									console.log(par[0] + ' : ' + par[1]);
								}

								$.ajax({
									url: 'peticiones.php?op=1',
									dataType: 'text',
    								data: datosEnviar,
    								type: 'POST',
								    contentType: false,
    								processData: false, 
    								success: function(r) {
        								successAct(r);
									},
									error: function(respuesta) {
										console.log('Error en ajax');
									}
								});

								//Función success actualización
								function successAct(respuesta) {
									if (respuesta == 'ok')
									{
										// Sacamos los campos de texto y lo dejamos como textos.
										// Recorremos las casillas y las convertimos en campos de texto.
										padre.find("td").slice(0, 5).each(function()
										{
											$(this).html($("input", this).val());
										});

										padre.find("td").slice(0, 5).css("background", "green");
									}
									else
									{
										padre.find("td").slice(0, 5).css("background", "red");
									}
								}
								
							}
						});
					}
				});

				// Primero asignar los eventos de esta forma.
				// $("img[src*='close']").click(function()
				// Luego ponerlo con .delegate para que valga para los nuevos registros añadidos.
				// En .on hay que poner como segundo parametro el selector específico.
				// Ya que el evento burbujea hasta llegar al padre.
				$("#milistado").on("click", "img[src*='close']", function()
				{
					if ($("input").length != 0)
					{
						padre.html(contenidoOriginalFila);
					}

					if (confirm("¿Desexa borrar este rexistro?"))
					{
						padre = $(this).parent().parent();
						console.log(padre.children().first())
						var codigoPais = padre.find("td:eq(0)").text();

// Hacemos la petición Ajax de actualización.

}
});



				$("img[src*='add']").click(function()
				{
					// Comprobamos si hay alguna otra fila en Edición.
					if ($("input#edicion").length != 0)
					{
						padre.html(contenidoOriginalFila);
					}


					$("img[src*='add']").hide();

					// Creamos una fila nueva con campos de formulario.
					$("<tr><td><input type='text'/></td><td><input id='altas' type='text'/></td><td><input type='text'/></td><td><input type='text'/></td><td><input type='text'/></td><td> <img src='img/edit.png'/></td><td><img src='img/close.png'/></td></tr>").insertAfter("tr:eq(0)");

					$("input").keyup(function(evento) {
						if (evento.which == 13)
						{
							// Hacemos la petición Ajax de actualización.
							$.post("peticiones.php?op=3", {nombrepais: $("input:eq(0)").val(), region: $("input:eq(1)").val(), area: $("input:eq(2)").val(), poblacion: $("input:eq(3)").val(), gdp: $("input:eq(4)").val()}, function(respuesta) {

								if (respuesta == 'ok')
								{
									$("img[src*='add']").show();
									// Recorremos las casillas y las convertimos en campos de texto.
									$("tr:eq(1) td").slice(0, 5).each(function()
									{
										$(this).html($("input", this).val());
									});

									$("tr:eq(1) td").slice(0, 5).css("background", "green");
								}
								else
								{
									// Ponemos fondo naranja de advertencia, de que ha habido un error.
									$("tr:eq(1) td").slice(0, 5).css("background", "red");
								}

							});
						}
					});
				});

});
</script>
</body>
</html>