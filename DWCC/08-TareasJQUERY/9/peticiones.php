<?php
require("conexion.php");

$sql = sprintf("select * from paises2 order by %s %s limit %s,%s", $mysqli->real_escape_string($_GET['campo']),$mysqli->real_escape_string($_GET['ordenacion']),$mysqli->real_escape_string($_GET['limiteinf']),$mysqli->real_escape_string($_GET['limitesup']));


$resultados=$mysqli->query($sql);

// Creamos un array.
$json= array();

// Recorremos el recordset y metemos los resultados en un array.
while ($fila=$resultados->fetch_assoc())
{
	$json[]=$fila;
}

echo json_encode($json);

?>