//Aqui se realizarán las validaciones del formulario

window.onload=iniciar;

/*Se crea un contador para las cookies*/
var contadorIntentos=0;
//Se almacenará de la siguiente manera la información de la cookie.
document.cookie="Intento de Envíos del formulario: 0";


var stringJSON = "";


function iniciar(){
          
    document.getElementById("enviar").addEventListener('click',validar,false);
    document.getElementById("limpiar").addEventListener('click',limpiar,false);
    //document.getElementById("errores").innerHTML="";


    /*Cuando el campo NOMBRE pierda el foco*/
    document.getElementById("nombre").onblur = function(){
        mayusculas("nombre");        

    };
    /*Cuando el campo APELLIDO pierda el foco*/
    document.getElementById("apellidos").onblur = function(){
        mayusculas("apellidos"); 
 
    };
};
/*FUNCION BOTON LIMPIAR*/
function limpiar(){
    document.getElementById("errores").innerHTML="";
    let formulario = document.getElementById("formulario");
    for(let i =0;i<formulario.elements.length;i++){
        //formulario.elements[i].className="";
        formulario.elements[i].classList.remove("error");
    }
}


/*FUNCION BOTON ENVIAR*/
function validar(eventopordefecto){
   
    //De esta manera solo llamo una vez a la función, y podré comprobar más abajo que es false si le doy a "cancelar" (Linea 65)
    let confirmacion = pedirConfirmacion();
    

    if(validarCamposTexto(this)&&validarNombre()&&validarApellidos()&&validarEdad()&&validarNIF()&&validarEmail()&&validarProvincia()&&validarFechaNac()&&validarTelefono()&&validarHora()&&confirmacion){ 
        /*Si se consigue acceder, limpiamos el numero de intentos*/
        //(BORRAR ESTO SI QUEREMOS QUE EL INTENTO PERDURE - DESCOMENTARLO EN CASO CONTRARIO)
        /*contadorIntentos=0;
        document.cookie="Intento de Envíos del formulario: "+contadorIntentos;        
        document.getElementById("intentos").innerHTML="";*/
        /*-------------------------------------------------------------------------------------------------------*/
        //Limpio los posibles errores anteriores:
    
        document.getElementById("errores").innerHTML="";
        return true;        
    }else{
       
        

        /*La cookie se incrementa cuando hay error*/
        contadorIntentos++;
        document.cookie="Intento de Envíos del formulario: "+(contadorIntentos);
        //Se añade el string que se almacena en la cookie al div        
        document.getElementById("intentos").innerHTML=document.cookie;



         //Si confirmacion es false no incremento el numero de intentos
        if(confirmacion==false){
            contadorIntentos--;
            console.log(contadorIntentos);

            if(contadorIntentos==0){
                document.getElementById("intentos").innerHTML="";     
            }else{
                document.cookie="Intento de Envíos del formulario: "+(contadorIntentos);
                document.getElementById("intentos").innerHTML=document.cookie; 
            }
            document.getElementById("errores").innerHTML="";
        }
  
        eventopordefecto.preventDefault();
        return false;
    }
}

/*------------------------------------- */


/*Funcion que comprueba que los campos de texto no están vacios*/
function validarCamposTexto(objeto){        
    let formulario = objeto.form;
    for(let i =0;i<formulario.elements.length;i++){
        //formulario.elements[i].className="";
        formulario.elements[i].classList.remove("error");
    }
    for(let i=0;i<formulario.elements.length;i++){
        if(formulario.elements[i].type=="text"&&formulario.elements[i].value==""){
            //alert("El campo "+formulario.elements[i].name+" no puede estar en blanco");
            document.getElementById("errores").innerHTML="Campo "+formulario.elements[i].name+" vacío.";
            //formulario.elements[i].className="error";
            formulario.elements[i].classList.add("error");
            formulario.elements[i].focus();
            return false;
        }
    }    
    return true;
}


/*Funcion para validar el nombre*/
function validarNombre(){
    
    //document.getElementById("errores").innerHTML="";
    let campoNombre = document.getElementById("nombre");
    let nombre = eliminaAcentos(campoNombre.value);
    
    

    let patron = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ]{0,30}$/;


    if(contieneNumeros(nombre)==true && patron.test(nombre)==false){
        document.getElementById("errores").innerHTML="El campo nombre no puede contener números";
        campoNombre.classList.add("error");
        campoNombre.focus();
        return false;       
    }else{
        //document.getElementById("errores").innerHTML="";
        return true;
    }
}    
/*Función para validar los apellidos*/
function validarApellidos(){
    let campoApell = document.getElementById("apellidos");
    let apellidos = eliminaAcentos(campoApell.value);
    console.log(apellidos);

    let patron = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]{0,40}$/;
    if(contieneNumeros(apellidos)==true && patron.test(apellidos)==false){
        document.getElementById("errores").innerHTML="El campo apellidos no puede contener números";
        campoApell.classList.add("error");
        campoApell.focus();
        return false;       
    }else{
        //document.getElementById("errores").innerHTML="";
        return true;
    }

}
/*Funcion para validar la edad*/
function validarEdad(){
    let campoEdad = document.getElementById("edad");
    let edad = campoEdad.value;

    let patron=/^[0-9]{0,3}$/;
    if(contieneNumeros(edad)==true&&patron.test(edad)&&edad<=105&&edad>=0){
        return true;
    }else{
        document.getElementById("errores").innerHTML="El campo edad tiene que ser un número entre 0 y 105";
        campoEdad.classList.add("error");
        campoEdad.focus();
        return false;
    }
}
/*Funcion para validar el NIF*/
function validarNIF(){
    let campoNIF = document.getElementById("nif");
    let nif = campoNIF.value;
    
    let patron = /^[0-9]{8}-[a-zA-Z]{1}$/;
        //[0-9]Un numero de 0 a 9
        //{7}Que se repite 8 veces
        //- Un guión
        //[a-zA-Z] Y una letra mayuscula o minúscula
        //{1} La letra solo puede estar 1 vez
    if(patron.test(nif)){
        return true;
    }else{
        document.getElementById("errores").innerHTML="El campo NIF debe tener el formato: NNNNNNN-L (7 números y 1 letra separada por un guión)";
        campoNIF.classList.add("error");
        campoNIF.focus();
        return false;
    }

}
/*Funcion validar email*/
function validarEmail(){
    let campoEmail = document.getElementById("email");
    let email = eliminaAcentos(campoEmail.value);

    let patron = /^[a-zA-Z\_\.\-0-9]{1,40}@[a-zA-Z0-9]{1,20}.[a-zA-Z]{1,3}$/
        //PARA EL USUARIO:
        //[a-zA-Z\_\.\-0-9] Letras mayusculas o minusculas,guiones,barras,puntos y numeros.
        //{1,40}De 1 hasta 40 veces para el nick.

        //@ Un arroba. QUE SEPARA USUARIO DE DOMINIO

        //PARA EL DOMINIO:
        //[a-zA-Z0-9]Letras y numeros
        //{1,20} Que se repitan de 1 a 20.
        //. Un punto
        //[a-zA-Z]Letras
        //{1,3}Que se repitan de 1 a 3 veces.

    if(patron.test(email)){
        return true;
    }else{
        document.getElementById("errores").innerHTML="El campo email debe de tener el siguiente formato: usuario@dominio.xxx";
        campoEmail.classList.add("error");
        campoEmail.focus();
        
        return false;
    }


}       

/*Funcion validar provincia*/
function validarProvincia(){
    let campoProv = document.getElementById("provincia");
    

    if(campoProv.selectedIndex==0){
        document.getElementById("errores").innerHTML="Debes seleccionar una provincia";
        campoProv.classList.add("error");
        campoProv.focus();

        return false;
    }else{
        return true;
    }

}

/*Funcion validar Fecha Nacimiento*/
function validarFechaNac(){
    let campoFechNac=document.getElementById("fecha");
    let fechaNacimiento = campoFechNac.value;

    let patron1 = /^[0-3]{1}[0-9]{1}[\/]{1}[0-1]{1}[0-9]{1}[\/]{1}[0-9]{4}$/;
    let patron2 = /^[0-3]{1}[0-9]{1}[\-]{1}[0-1]{1}[0-9]{1}[\-]{1}[0-9]{4}$/;
    //[0-3]{1}[0-9]{1} Con este framgento me aseguro de que sea un numero entre 01 y 39
    //[\/]{1} Seguido de una barra o [\-]{1} Seguido de un guión.
    //[0-1]{1}[0-9]{1} Un numero entre 0 y 19
    //[\/]{1} Seguido de una barra o [\-]{1} Seguido de un guión.
    //[0-9]{4} Un numero de 4 dígitos

    if(patron1.test(fechaNacimiento) || patron2.test(fechaNacimiento)){
        //fechaNacimiento=fechaNacimiento[0]+fechaNacimiento[1]+"/"+fechaNacimiento[3]+fechaNacimiento[4]+"/"+fechaNacimiento[6]+fechaNacimiento[7]+fechaNacimiento[8]+fechaNacimiento[9];
        return true;
    }else{
        document.getElementById("errores").innerHTML="La fecha debe tener el siguiente formato: dd/mm/aaaa o dd-mm-aaaa"
        campoFechNac.classList.add("error");
        campoFechNac.focus();
        return false;
    }


}
/*Funcion validar Telefono*/
function validarTelefono(){
    let campoTelefono = document.getElementById("telefono");
    let telefono = campoTelefono.value;

    patron = /^[0-9]{9}$/;
    //[0-9]{9} Un numero de 0 a 9 que se repita 9 veces.

    if(patron.test(telefono)&&contieneNumeros(telefono)==true){
        return true;
    }else{
        document.getElementById("errores").innerHTML="El campo telefono debe de ser de 9 números"
        campoTelefono.classList.add("error");
        campoTelefono.focus();
        return false;
    }


}
/*Funcion validar Hora*/
function validarHora(){
    let campoHora = document.getElementById("hora");
    let hora = campoHora.value;

    patron = /^[0-9]{2}:[0-9]{2}$/;
    //[0-9]{2} Un numero de 0 a 9 que se repita 2 veces
    //: Dos puntos
    //[0-9]{2} Un numero de 0 a 9 que se repita 2 veces

    if(patron.test(hora)){
        return true;
    }else{
        document.getElementById("errores").innerHTML="El campo hora debe tener el siguiente formato: mm:hh"
        campoHora.classList.add("error");
        campoHora.focus();
        return false;
    }

}
/*Funcion perdir confirmacion*/
function pedirConfirmacion(){
    if(confirm("¿Deseas enviar el formulario?")==true){

        return true;
    }else{
        
        return false;
    }
}




/*Funcion que indica si un string contiene numeros o no*/
function contieneNumeros(string){
    let contadorNumeros=0;
    if(isNaN(parseInt(string))){       
        for(let i=0;i<string.length;i++){
            if(isNaN(parseInt(string[i]))==false){
                contadorNumeros++;
            }
        }        
        if(contadorNumeros>0){
            return true;
        }else{
            return false;
        }
    }else{
        return true;
    }
}


/*Funcion que se utilizará para pasar a mayusculas NOMBRE y APELLIDOS (Línea 11 y 15)*/
function mayusculas(id){
    let elemento = document.getElementById(id);
    elemento.value = eliminaAcentos(elemento.value.toUpperCase());
}

/*Funcion que elimina acentos y ñ*/

function eliminaAcentos(string){

    let diccionario = {
            "á":"a","é":"e","í":"i","ó":"o","ú":"u","ñ":"n",
            "Á":"A","É":"E","Í":"I","Ó":"O","Ú":"U","Ñ":"N",
            "à":"a","è":"e","ì":"i","ò":"o","ù":"u","ä":"a",
            "À":"A","È":"E","Ì":"I","Ò":"O","Ù":"U","Ä":"A"
        
        }
    let patron = /[áéíóúàèìòùñä]/ig;
    let resultado = string.replace(patron,function(e){return diccionario[e]});
    return resultado;
}

