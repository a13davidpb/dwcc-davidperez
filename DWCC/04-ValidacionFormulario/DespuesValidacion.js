

function goBack() {
window.history.back();
}

//Con la siguiente línea obtengo la URL como un string 
var stringURL = window.location.href;//(Esto da problema con acentos y Ñ, lo corregí eliminando acentos y Ñ)


/*Funcion que encontré por internet para obtener variables de la URL*/
function getURL(variable){
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    //console.log(query);
    for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

/*
console.log(getURL("nombre"));
console.log(getURL("apellidos"));
console.log(getURL("edad"));
console.log(getURL("nif"));
console.log(getURL("email").split("%40").join("@"));
console.log(getURL("provincia"));
console.log((getURL("fecha").split("%2F")).join("/"));
console.log(getURL("telefono"));
console.log((getURL("hora").split("%3A")).join(":"));*/


var $nombre = getURL("nombre");
var $apellidos = getURL("apellidos").split("+").join(" ");
var $edad= getURL("edad");
var $nif= getURL("nif");
var $email= getURL("email").split("%40").join("@");
var $provincia= getURL("provincia");
var $fecha= (getURL("fecha").split("%2F")).join("/");
var $telefono= getURL("telefono");
var $hora=(getURL("hora").split("%3A")).join(":");


if($provincia=="C"){
    $provincia="A Coruña";    
}
if($provincia=="LU"){
    $provincia="Lugo";
}
if($provincia=="OU"){
    $provincia="Ourense";
}
if($provincia=="PO"){
    
    $provincia="Pontevedra";
}



/*//Imprimo las variables anteriores
console.log($nombre);
console.log($apellidos);
console.log($edad);
console.log($nif);
console.log($email);
console.log($provincia);
console.log($fecha);
console.log($telefono);
console.log($hora);*/

//Creo un objeto diccionario:
var obj = { nombre:$nombre,apellidos:$apellidos,edad:$edad,nif:$nif,email:$email,provincia:$provincia,fecha:$fecha,telefono:$telefono,hora:$hora};
//Creo una variable JSON:
var myJSON = JSON.stringify(obj);
document.getElementById("json").innerHTML=myJSON;
