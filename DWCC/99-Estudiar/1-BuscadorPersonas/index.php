<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buscador personas</title>
    <style>
        body{
            background-color:#8fcd70;
        }
        div.zonaconborde{
            position:absolute;
            top:3.5cm;
            left:0;
            right:1;
            margin:auto;
            width:153px;
            background-color:white;
            text-align:left;
            border:1px solid black;
            cursor:pointer;
            margin-left:10%;         
        }
        li{
            padding:5px;
            list-style:none;
        }
        .enlace_sugerencia_over{
            background-color:orange;
        }

        .error{
            background-color:#F19BB7;
            border: 2px solid #AA3100;
            color:#71331A;
            border-radius:15px;
            width:20cm;
            text-align:center;
            margin:1cm;
            padding:0.2cm;
        }

        .bien{
            background-color:#BDFF48;
            border: 2px solid #7Ab314;
            color:#4c6128;
            border-radius:15px;
            width:20cm;
            text-align:center;
            margin:1cm;
            padding:0.2cm;

        }


        .editar{
            background-color:#CDCDCD;
            padding:0.2cm;
            color:black;
            border-radius:15px;
            border:1px solid black;
            margin-left:1cm;
        }
        .editar:hover{
            background-color:#9b9b9b;
            cursor:pointer;
        }

    </style>


</head>
<body>
    <h1>Buscador de personas</h1>
    Introduce un nombre: <input type="text" id="cajatexto"></input> <input id="buscar" type="button" value="buscar">

    <div id="sugerencias"></div>
    <div id="mensaje"></div>
    <div id="datos"></div>
    <a type="button" id="editar" class="editar" />Editar</a>

<script src="js/jquery.js"></script>
<script>

    $(document).ready(function(){//Inicio document.ready
        //Evitar browser cache
        $.ajaxSetup({cache:false});

        //Oculto el enlace de editar
        $("#editar").hide();

                       

        //Cada vez que hago click en la caja de texto:
        $("#cajatexto").on("click",function(){
            //Vaciar caja texto
            $("#cajatexto").val("");

            //Cada vez que pulso tecla en caja de texto
            $("#cajatexto").keyup(function(){
                //Se crea una variable con el contenido de la caja
                var stringSugerir = $.trim($(this).val());      

                //Preparar los datos a enviar por post:
                var datosEnviar = new FormData();
                datosEnviar.append("nombreSugerir",stringSugerir);

                //Petición por ajax:
                $.ajax({
                    url: 'crud.php',
                    dataType: 'text',
                    data: datosEnviar,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function(r){
                            successAct(r);
                    },
                    error: function(respuesta){
                        console.log("ERROR AJAX");
                    }
                });//FIN petición por ajax

                //Funcion successAct de ajax
                function successAct(respuesta){
                    
                    //Añado las sugerencias al div de sugerencias:
                    if(respuesta!=""){
                        $("#sugerencias").html(respuesta).addClass("zonaconborde");
                    }else{
                        $("#sugerencias").html(respuesta).removeClass("zonaconborde");
                    }//Fin añadir sugerencias al div


                    //Cada vez que paso el raton por encima de una sugerencia:
                    $("#sugerencias li").each(function(){
                        
                        //Cada vez que pasamos el raton por encima de una sugerencia y lo quitamos:
                        $(this).mouseover(function(){
                            $(this).addClass("enlace_sugerencia_over");
                        });
                        $(this).mouseout(function(){
                            $(this).removeClass("enlace_sugerencia_over");
                        });//FIN Cada vez que pasamos el raton por encima de una sugerencia y lo quitamos

                        //Eliminamos las sugerencias al hacer click en una
                        $(this).on("click",function(){
                            $("#cajatexto").val($(this).text());
                            $("#sugerencias").html("").removeClass("zonaconborde");
                        })//FIN ELIMINAR SUGERENCIAS AL HACER CLICK

                        //Eliminamos las sugerencias al hacer click en cualquier parte
                        $("*").on("click",function(){                            
                            $("#sugerencias").html("").removeClass("zonaconborde");
                        })



                    });//Fin cada sugerencia




                }//Fin funcion successAct de ajax

            });//FIN Cada vez que se pulsa una letra

        });//FIN click en caja de texto
        


        //CADA VEZ QUE HAGO CLICK EN EL BOTON BUSCAR:
        $("#buscar").on("click",function(){            
            var nombre = $("#cajatexto").val();


            $.post("./crud.php",{nombreBuscar:nombre},function(objetoUsuario){

                if(objetoUsuario.status=="usuarionoencontrado"){ //SI NO SE ENCUENTRA EL USUARIO                  
                    $("#mensaje").html("NO SE HA ENCONTRADO EL USUARIO").addClass("error").fadeIn(3000).delay(3000).fadeOut(1000);
                    $("#editar").hide();
                    $("#datos").html("");
                }else{//USUARIO ENCONTRADO

                    console.log(objetoUsuario);

                    var datosUsuario = `
                        <ol id="olresultado">
                            <li><b>Nombre</b> <div id="nombre">`+objetoUsuario["nombre"]+`</div></li>
                            <li><b>Dirección</b> <div id="direccion">`+objetoUsuario["direccion"]+`</div></li>
                            <li><b>Tarjeta Crédito</b> <div id="tarjeta">`+objetoUsuario["tarcredito"]+`</div></li>
                        <ol>
                        <br> 
                    `;

                    $("#editar").show();
                    $("#datos").html(datosUsuario);

                    //CLICK EN EL BOTON EDITAR:
                    $("#editar").on("click",function(){
                        $("#editar").hide();

                        padre = $("#olresultado");

                        idOriginal = padre.find("li:eq(0)").text();

                        console.log(idOriginal);
                        contenidoOriginal = padre.html();

                        //Inserto los inputs
                        padre.find("div").slice(1,3).each(function(){
                            $(this).html("<input class='selector edicion' style='width:7cm' type='text' value='"+$(this).text().trim()+"'/>");
                        });


                        nombreeditar = $("#nombre").html();


                        //Creo el evento al hacer intro:
                        $("input").keyup(function(evento){
                            //SI ES INTRO:
                            if(evento.which==13){
                                var datosEnviar = {
                                    nombre:nombreeditar,
                                    direccion:$(".selector:eq(0)").val(),
                                    tarjeta:$(".selector:eq(1)").val(),
                                };
                                
                                console.log(datosEnviar);
                                $.post("./crud.php",datosEnviar,function(respuesta){
                                    if(respuesta=="usuariomodificado"){
                                        $("#mensaje").html("USUARIO MODIFICADO").addClass("bien").fadeIn(3000).delay(3000).fadeOut(1000);
                                                                                
                                            $("#direccion").html(datosEnviar["direccion"]);
                                            $("#tarjeta").html(datosEnviar["tarjeta"]);
                                            $("#editar").show();

                                    };
                                });




                            }//FIN INTRO
                        })//FIN EVENTO



                    });//FIN CLICK EDITAR

                }//FIN USUARIO ENCONTRADO

            });//FIN POST

            
        })//FIN CLICK BOTON BUSCAR


    });//FIN document.ready
    
</script>
    
</body>
</html>