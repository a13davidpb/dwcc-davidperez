<?php

require_once "conexion.php";

//CONSULTA INICIAL
if(isset($_GET["consulta"])){
    
    $stmt=$pdo->prepare("select * from personas order by nombre asc");
    $stmt->execute();


    $botonEditar = "<a class='btneditar'>Editar</a>";
    $botonEliminar = "<a class='btneliminar'>Eliminar</a>";



    while($fila=$stmt->fetch()){
        $foto = $fila["foto"];

        //Si es null la foto en la base de datos pongo una por defecto:
        if($foto == NULL){
            $foto = "<img src='./imagenes/default.jpg'/>";
        }


        echo "<tr><td>".$fila["nombre"]."</td><td>".$foto."</td><td>$botonEditar</td><td>$botonEliminar</td></tr>";
    }
}//FIN CONSULTA INICIAL


//CONSULTA MODIFICAR FOTO
if(isset($_POST["nombreusuario"])&&isset($_POST["nombrefoto"])){

    $nombreFoto = $_POST["nombrefoto"];
    $nombreUsuario = $_POST["nombreusuario"];
    
    $stmt = $pdo->prepare("update personas set foto=:fotousuariomod where nombre='$nombreUsuario'");
    //$stmt = $pdo->prepare("update personas set foto='<img src=:fotousuariomod/>' where nombre='$nombreUsuario'");
    
    $fileTmpPath = $_FILES["fotoenviar"]["tmp_name"];
    $fileName = $_FILES["fotoenviar"]["name"];
    $fileSize = $_FILES["fotoenviar"]["size"];
    $fileType = $_FILES["fotoenviar"]["type"];
    $fileNameCmps = explode(".",$fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    $newFileName = md5(time().$fileName).'.'.$fileExtension;

    $extensionspermitidas=array('jpg','gif','png');

    if(in_array($fileExtension,$extensionspermitidas)){
        $uploadFileDir = './imagenes/';
        $dest_path = $uploadFileDir.$newFileName;
        move_uploaded_file($fileTmpPath,$dest_path);
        $img = "<img src='".$dest_path."'/>";
        $stmt->bindValue(':fotousuariomod',$img);
    }else{
        echo "ERROR";
    }
    $stmt->execute();
    echo "SUBIDA";

    
    
}//FIN MODIFICAR FOTO




?>