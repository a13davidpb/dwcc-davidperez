<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Personas</title>
    <style>
        
        th{
            width:4cm;
            
        }
        tr{
            height:2cm;
            text-align: center;
        }
        .btneditar{
            background-color:grey;
            padding:5px;
            color:white;
            border:2px solid black;
            border-radius:10px;
        }
        .btneditar:hover{
            background-color:#D4D4D4;
            color:black;
            cursor:pointer;
        }
        .btneliminar{
            background-color:grey;
            padding:5px;
            color:white;
            border:2px solid black;
            border-radius:10px;
        }
        .btneliminar:hover{
            background-color:#D4D4D4;
            color:black;
            cursor:pointer;
        }

        .btnok{
            
            background-color:#458c36;
            padding:5px;
            color:white;
            border:2px solid black;
            border-radius:10px;
        }
        .btnok:hover{
            background-color:#9ee98d;
            color:black;
            cursor:pointer;
        }
        img{
            width:2cm;
        }





    </style>
</head>
<body>
    <h1>Listado de personas y fotos</h1>
    <div id="listado"></div>

    <table id='tabla' border=1>
        <tbody>
            <tr>
                <th>Nombre</th>
                <th>Foto</th>
                <th colspan="2">Edición</edicion>
            </tr>
        </tbody>
    </table>
    
    <script src='./js/jquery.js'></script>
    <script>
        $(document).ready(function(){//DOCUMENT READY

            $.ajaxSetup({cache:false});

            //Consulta de personas:
            var consulta = "OK";            

            $.get("./crud.php",{consulta:consulta},function(respuesta){
                
                $("tbody").append(respuesta);


                var contenidoOriginalFila;
                //Editar personas:
                //$(".btneditar").on("click",function(){ //->Esta forma no funciona bien
                    $("table").on("click",".btneditar",function(){

                    if($(".fotousuario").length!=0){
                        fila.html(contenidoOriginalFila);
                        //location.reload();
                        
                    }

                   
                    fila = $(this).parent().parent();
                    nombre=fila.find("td:eq(0)").text();

                   
                    contenidoOriginalFila = fila.html();
                    
                   

                    if(confirm("¿Editar?")){//CONFIRMACION EDITAR
                        fila.find("td").eq(1).html("<input class='fotousuario' type='file' name='fotousuariomod' accept='image/*'/>");                        
                        fila.find("td").eq(2).html("<a class='btnok'>OK</a>");

                        //BOTON OK
                        $(".btnok").on("click",function(){
                            let datosEnviar = new FormData();
                            datosEnviar.append('nombrefoto',$("input[type=file]")[0].files[0]["name"]);
                            datosEnviar.append('fotoenviar',$("input[type=file]")[0].files[0]);
                            datosEnviar.append('nombreusuario',nombre);

                            //Ver lo que se envia:
                            for(var par of datosEnviar.entries()){
                                console.log(par[0]+" : "+par[1]);
                            }

                            //PETICION AJAX
                            $.ajax({
                                url:"crud.php",
                                dataType:'text',
                                data: datosEnviar,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(r){
                                    successAct(r);
                                },
                                error: function(respuesta){
                                    console.log("ERROR AJAX");
                                }
                            });//FIN PETICION AJAX

                            //FUNCION AJAX:
                            function successAct(respuesta){
                                //console.log(respuesta);
                                if(respuesta=="SUBIDA"){
                                    location.reload();
                                }
                            }//FIN FUNCION AJAX


                        })//FIN BOTON OK

                    }//FIN CONFIRMACION EDITAR

                });//Editar



            });//FIN CONSULTA DE PERSONAS


        });//FIN DOCUMENT READY


    </script>





</body>
</html>