<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Paises</title>
    <style>
     .addpais{
		 margin:5px;
	 }
	 table{
		 margin-top:20px;
	 }
	 td, th{
		 /*background-color:#CBCBCB;*/
	 }
	 .bien{
		 /*background-color:#9ed17e;*/
		 background:lightgreen;
	 }
	 .mal{
		 background:#E48067;
	 }
    
    </style>

</head>
<body>

	<h1>Listado completo de paises</h1><div id="añadir"></div>
	<div id="listado"></div>

	<table id="table" border="1">
		<tbody>
			<tr id='encabezado'><th>País</th><th>Región</th><th>Área</th><th>Población</th><th>GDP</th><th colspan="2">Edición</th></tr>
		</tbody>
	</table>



    <script src="js/jquery.js"></script>
    <script>
		//DOCUMENT READY
        $(document).ready(function(){
            $.ajaxSetup({chache:false});

			var creaListado = "listado";

			$.get("./crud.php", {crealistado:creaListado},function(respuesta){
		
				$("tbody").append(respuesta);
				$("#añadir").html("<b class='addpais'>Añadir un nuevo país</b><img id='add' src='./img/add.png'/>");
				$("img").css("cursor","pointer");
			
			
				var contenidoOriginalFila;//Tuve que declarar esta variable fuera

				//Editar, cada vez que se hace click en el icono editar:
				//$("img[src*='edit'").on("click",function(){
					$("table").on("click","img[src*='edit']",function(){
						$("#add").hide();
						$(".addpais").hide();
						
						//Si se quiere editar otro registro no editar dos a la vez:				
						if($("input.edicion").length>0){
							fila.html(contenidoOriginalFila);							
						}	
				
						if(confirm('¿Editar?')){//CONFIRMACION

							//FILA ENTERA
							fila = $(this).parent().parent();
				

							//Nombre del pais de la fila (Primera celda)
							nombrePais = fila.find("td:eq(0)").text();
							

							//Contenido original de la fila
							contenidoOriginalFila = fila.html();							

							
							//Convertimos las celdas de la fila a inputs:
							fila.find("td").slice(0,5).each(function(){
								$(this).html("<input type='text' class='edicion' value='"+$(this).text()+"' />");
							})//Fin cada celda de fila a input		

							//Al hacer intro en un campo imput
							$("input").keyup(function(evento){
								if(evento.which==13){
									
									var datosEnviar = {
										nombrepais:$("input:eq(0)").val(),
										region:$("input:eq(1)").val(),
										area:$("input:eq(2)").val(),
										poblacion:$("input:eq(3)").val(),
										gdp:$("input:eq(4)").val(),
										paisoriginal:nombrePais
									}
									//Enviamos datos actualizacion
									$.post("./crud.php",datosEnviar,function(respuesta){
										console.log(respuesta);
											if(respuesta=="MODIFICADO"){
												//location.reload();
												//En vez de hacer un reload, modifico los campos, asi no da sensacion de recarga:
												fila.find("td").slice(0,5).each(function(){
													$(this).text($(this).find("input").val());
												});
												
												//Pongo fondo verde (Si la tabla tiene un color de fondo esto puede no funcionar bien)
												fila.addClass('bien').delay(1000).queue(function(){
													$(this).removeClass('bien');
												})
											}
									})//Fin actualiuzacion
								}
							})//Fin hacer intro
						}//FIN CONFIRMACION
				})//FIN EDITAR	



				//ELIMINAR PAIS
				$("table").on("click","img[src*='close']",function(){
					
							//FILA ENTERA
							fila = $(this).parent().parent();
				

							//Nombre del pais de la fila (Primera celda)
							nombrePaisEliminar = fila.find("td:eq(0)").text();							

							$.post("./crud.php",{paiseliminar:nombrePaisEliminar},function(respuesta){								
								if(respuesta == "PAISELIMINADO"){
									fila.addClass('mal').delay(1000).queue(function(){
											location.reload();
									});
								};
							});
				});//FIN ELIMINAR PAIS	

				//AÑADIR PAIS NUEVO
				$("#add").on("click",function(){
						$("#add").hide();
						$(".addpais").hide();
						$("#encabezado").hide();
						$("table").append(`
							<thead>
								<tr>
									<th>Nombre</th><th>Región</th><th>Área</th><th>Población</th><th>GDP</th>
								</tr>
								<tr>
									<td><input style='width:90%'type='text'/></td>
									<td><input style='width:90%'type='text'/></td>
									<td><input style='width:90%'type='text'/></td>
									<td><input style='width:90%'type='text'/></td>
									<td><input style='width:90%'type='text'/></td></tr>
							</thead>`);
						
						//Al hacer intro en un campo imput
						$("input").keyup(function(evento){
							if(evento.which==13){

								fila=$(this).parent().parent();

								contenidoNuevoEnviar ={
									addpais:$("input:eq(0)").val(),
									addregion:$("input:eq(1)").val(),
									addarea:parseFloat($("input:eq(2)").val()),
									addpoblacion:parseFloat($("input:eq(3)").val()),
									addgdp:parseFloat($("input:eq(4)").val()),
								} 




								
								
								$.post("./crud.php",contenidoNuevoEnviar,function(respuesta){
									console.log(respuesta);
									if(respuesta=='PAISAGREGADO'){
										location.reload();
									}
								})


							}


						})


				});
							

			});//FIN CONSULTA
			

			
        });//FIN DOCUMENT READY
    </script>    
</body>
</html>