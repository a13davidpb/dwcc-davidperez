<?php
//header('Content-type: application/json');

require_once "./conexion.php";

//SUGERENCIA DE CONCELLOS
if(isset($_GET["nombreConcelloSugerir"])){

    //$stmt=$pdo->prepare("select concello from casadoconcello where concello like :busqueda");
    $stmt=$pdo->prepare("select distinct concello from praiasbazuis where concello like :busqueda");
    $stmt->bindValue(':busqueda','%'.$_GET['nombreConcelloSugerir'].'%');
    $stmt->execute();

    while($fila=$stmt->fetch()){
        echo "<li>".$fila['concello']."</li>\n";
    }
};//FIN SUGERENCIA DE CONCELLOS


//BUSQUEDA DE CONCELLO
if(isset($_GET["nombreConcelloBuscar"])){
    header('Content-type: application/json');
    $stmt=$pdo->prepare("select * from casadoconcello where concello=:concelloBuscar");
    $stmt->bindParam(':concelloBuscar',$_GET["nombreConcelloBuscar"]);

    $stmt->execute();
    if($stmt->rowCount()!=0){
        echo json_encode($stmt->fetch());
    }else{
        echo '{"status":"concellonoencontrado"}';
    }
	
}//FIN BUSQUEDA CONCELLO


//BUSQUEDA DAS PLAYAS DO CONCELLO
if(isset($_GET["nombreConcelloBuscarPraias"])){
    $stmt=$pdo->prepare("select coordenadas from praiasbazuis where concello=:nombreConcello");
    $stmt->bindParam(':nombreConcello',$_GET["nombreConcelloBuscarPraias"]);
    $stmt->execute();

    if($stmt->rowCount()!=0){
        while($fila=$stmt->fetch()){
            echo $fila['coordenadas']."\n";
        }
    }else{
        echo "sinpraias";
    }

}//FIN BUSQUEDA PLAYAS CONCELLO


//OBTENER DATOS EXTRA DE PLAYA
if(isset($_GET["nombreConcelloBuscarDatosPraias"])){
    header('Content-type: application/json');
    $stmt=$pdo->prepare("select * from praiasbazuis where concello=:concelloBuscar");
    $stmt->bindParam(':concelloBuscar',$_GET["nombreConcelloBuscarDatosPraias"]);

    $stmt->execute();
    if($stmt->rowCount()!=0){
        $list = $stmt->fetchAll();
        echo json_encode($list);
    }else{
        echo '{"status":"error"}';
    }
    
}//FIN OBTENER DATOS EXTRA DE PLAYA


//METEOGALICIA
if(isset($_GET["COORDENADASMETEO"])){
    //Coordenadas recibidas (String):    
    $coordenadas = $_GET["COORDENADASMETEO"][1].",".$_GET["COORDENADASMETEO"][0];
    
    //PRUEBAS------------------------------------------------
    //$current_date = gmDate("Y-m-d\TH:i:s\Z");       
    //echo print_r($current_date,1); 
    //-------------------------------------------------------

    //Obtengo el string del dia actual:
        /*$diaActual=gmDate("Y-m-d"); //CON GMDATE DA UNA HORA MENOS
        $horaActual=gmDate("H:i:s");*/
        $diaActual=date("Y-m-d");
        $horaActual=date("H:i:s");
        $diaActual = $diaActual."T".$horaActual;//ESTO ES LO QUE QUIERO

        //echo $diaActual;

    
    //Sumo 3 dias a la fecha actual:
        $fecha_actual = date("Y-m-d");    
        $tres_dias = date("Y-m-d",strtotime($fecha_actual."+ 3 days"));//formato YYYY-MM-DD

        //echo print_r($tres_dias);


    //$htmlmeteogalicia  = "http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=".$coordenadas."&format=text/html&startTime=2020-02-16T10:00:00&endTime=2020-02-18T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN";
    $htmlmeteogalicia  = "http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=".$coordenadas."&format=text/html&startTime=".$diaActual."&endTime=".$tres_dias."T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN";
    
    echo $htmlmeteogalicia;
}//FIN METEOGALICIA

//OBTENER NOMBRE PLAYA A PARTIR DE COORDENADAS
if(isset($_GET["COORDENADASPLAYA"])){
    //echo $_GET["COORDENADASPLAYA"]; //Ejemplo -> %42.63075%,%-8.851667%
    $stmt=$pdo->prepare("select praia FROM praiasbazuis where coordenadas like :coordenadasBuscar");  
   
    $stmt->bindParam(':coordenadasBuscar',$_GET["COORDENADASPLAYA"]);

    $stmt->execute();

    $resultado = $stmt->fetchAll();
    echo ($resultado[0]["praia"]);

}//FIN OBTENER NOMBRE PLAYA A PARTIR DE COORDENADAS

//OBTENER TIEMPO PLAYA A PARTIR DE FECHAS INDICADAS
if(isset($_GET["CORDENADASPLAYAFECHAS"])&&isset($_GET["FECHAINICIO"])&&isset($_GET["FECHAFIN"])){
    //echo $_GET["CORDENADASPLAYAFECHAS"][0].",".$_GET["CORDENADASPLAYAFECHAS"][1]." - ".$_GET["FECHAINICIO"]." - ".$_GET["FECHAFIN"];
    $coordenadas = $_GET["CORDENADASPLAYAFECHAS"][1].",".$_GET["CORDENADASPLAYAFECHAS"][0];

    $diaActual=date("Y-m-d");

    $fechaInicio = $_GET["FECHAINICIO"];
    $fechaFin = $_GET["FECHAFIN"];

    $fecIniDate = strtotime($fechaInicio);
    $fecIniDate = date("Y-m-d H:i:s",$fecIniDate);

    $fecFinDate = strtotime($fechaFin);
    $fecFinDate = date("Y-m-d H:i:s",$fecFinDate);

    //Calcular 6 dias mas de la fecha actual(en esta fecha meteogalicia da error)
    $fecha_actual = date("Y-m-d");   
    $seis_dias = date("Y-m-d",strtotime($fecha_actual."+ 5 days"));//formato YYYY-MM-DD

    

    //echo ($fecIniDate>$diaActual);//Fecha inicio mayor a dia actual?
    //echo ($fecIniDate<$fecFinDate);//Fecha inicio es menor a fecha fin?

    if($fecIniDate>$diaActual!=true){
        echo "fechainiciomenordiaactual";
    }else if($fecIniDate===$fecFinDate){
        
                           /*http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=-8.882613,42.646987&format=text/html&startTime=2020-02-18T13:06:36&endTime=2020-02-21T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN*/
        $htmlmeteogalicia  = "http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=".$coordenadas."&format=text/html&startTime=".$fechaInicio."T01:00:00&endTime=".$fechaFin."T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN";

        echo $htmlmeteogalicia;

    }else if($fecIniDate<$fecFinDate!=true){
        echo "fechainiciomayorfechafinal";
    }else if($fecIniDate>=$seis_dias||$fecFinDate>=$seis_dias){
        echo "fueraderango";
    }else{      
                            /*http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=-8.882613,42.646987&format=text/html&startTime=2020-02-18T13:06:36&endTime=2020-02-21T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN*/
        $htmlmeteogalicia  = "http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?coords=".$coordenadas."&format=text/html&startTime=".$fechaInicio."T01:00:00&endTime=".$fechaFin."T23:59:00&API_KEY=9RLuizAk2VHYux3ZZ5Kr2141MFwTzlxC2cBdy74pPiI8MrDSc43JW4NIhwB231RN";

        echo $htmlmeteogalicia;
    }
    //LO DEJE AQUI.
}//FIN OBTENER TIEMPO DE PLAYA EN RANGO DE FECHA


?>