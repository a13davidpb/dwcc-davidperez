        //////////////////////////////////////////////////////////////////////////////////////////////////////
        const tilesProvider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
		let miMapa = L.map('mapa').setView([42.8802036,-8.5622792], 8);
		L.tileLayer(tilesProvider, {
			maxZoom: 18,
        }).addTo(miMapa);
        $("#mensaje").html("<h1>Praias de Galicia</h1>").addClass("titulo");
        //////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){
        //Evitar browser cache
        $.ajaxSetup({cache:false});

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        var marcadoresEliminar = []; //Array con todos los marcadores
        // Al hacer click en la caja de texto con id #concello vaciamos su contenido
        $("#concello").click(function() {
            $("#concello").val('');
            for (let m = 0; m < marcadoresEliminar.length; m++) {//Borramos todos los marcadores existentes en el mapa
                miMapa.removeLayer(marcadoresEliminar[m]); 
            }
            miMapa.setView([42.8802036,-8.5622792], 8);
            $("#zonainfo").html("");
            //Borro el mensaje de "Concello de....."
            $("#mensaje").html("<h1>Praias de Galicia</h1>").addClass("titulo");
        });
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        //Oculto el htmlmeteo y todo lo que tenga que ver con el
        $("#meteogalicia").hide();
        $("#tittiempo").hide();


        //Al hacer click en el input y escribir - SUGERENCIA
        $("#concello").click(function(){
            $("#mapa").show();
            $("#mensaje").show();
            //Oculto el htmlmeteo y todo lo que tenga que ver con el
            $("#meteogalicia").hide();
            $("#tittiempo").hide();

            //Vacio la caja:
            $(this).val("");
            $("#zonaresultado").html("").removeClass("zonaconborde");    

            //Cada vez que pulsamos una tecla debe ocurrir la sugerencia
            $("#concello").keyup(function(){
                

                var nombreConcelloBuscar =$.trim($(this).val());
                //Peticion al crud:
                $.get("./crud/crud.php",{nombreConcelloSugerir:nombreConcelloBuscar},function(datosDevueltos){
                    
                    if(datosDevueltos!=""){
                        $("#zonaresultado").html(datosDevueltos).addClass("zonaconborde");
                    }else{
                        $("#zonaresultado").html(datosDevueltos).removeClass("zonaconborde");
                    };


                     //Por cada elemento de la lista:
                     $("#zonaresultado li").each(function(){
                                
                        //Al pasar el raton por encima
                        $(this).mouseover(function(){
                            $(this).addClass("enlace_sugerencia_over");
                        });
                        //Al sacar el raton de encima
                        $(this).mouseout(function(){
                            $(this).removeClass("enlace_sugerencia_over");
                        });

                        //Cuando pincho en uno:
                        $(this).click(function(){
                            var concelloSeleccionado = $(this).text();
                            //Copiar la carrera en la caja de texto
                            $("#concello").val(concelloSeleccionado);
                            //Vacio la consulta
                            $("#zonaresultado").html("").removeClass("zonaconborde"); 
                                                               
                        });//Fin cuando se pincha en uno

                    });//Fin cada elemento

                });//Fin peticion crud                
                    
            });//Fin sugerencia

        });//Fin hacer click input y escribir

         //Al hacer click en el boton buscar - BUSQUEDA
        //Funcion boton click
        $("#buscar").on("click",function(){
            
            //BUSQUEDA DEL CONCELLO
            var concelloBuscar = $.trim($("#concello").val());            
            if(concelloBuscar!=""){

                //Peticion al crud del concello
                $.get("./crud/crud.php",{nombreConcelloBuscar:concelloBuscar},function(objetoConcello){
                    
                    
                    if(objetoConcello.status =="concellonoencontrado"){//SI NO ENCUENTRA EL CONCELLO
                        $("#mapa").hide();
                        $("#mensaje").hide();
                        $("#mensajeerror").addClass("error").text("No se ha encontrado el concello").fadeIn(1000).delay(500).fadeOut(2000);
                        

                    }else{//Si encuentra el conello:   
                       

                        //HAGO LA CONSULTA DE LAS PLAYAS DE ESE CONCLELO
                        var concello = objetoConcello["concello"];


                        //Latitud y longitud del concello
                        var latConcello = objetoConcello["lat"];
                        var lonConcello = objetoConcello["lon"];

                        //Array de coordenadas del concello
                        var coordenadasConcello = [parseFloat(latConcello),parseFloat(lonConcello)];
                        //console.log(coordenadasConcello);


                        //Nombre del concello
                        var nomConcello = objetoConcello["concello"].split(",");
                        nomConcello = nomConcello[0];
                        nomConcello = nomConcello.toLowerCase();
                        nomConcello=  nomConcello[0].toUpperCase()+nomConcello.slice(1); 


                        //PETICION DE LAS PLAYAS DEL CONCELLO
                        $.get("./crud/crud.php",{nombreConcelloBuscarPraias:concello},function(respuesta){
                                var arrayPraias=[];
                                if(respuesta!="sinpraias"){//SI ENCUENTRA PLAYAS:
                                    arrayPraias = respuesta.split("\n");   
                                    //CREO ARRAY DE PLAYAS                                 
                                    for(var i=0;i<arrayPraias.length-1;i++){
                                        //Creo un array de arrays de coordenadas de las playas
                                        arrayPraias[i]=arrayPraias[i].split(",");
                                        arrayPraias[i][0] = parseFloat(arrayPraias[i][0]);
                                        arrayPraias[i][1] = parseFloat(arrayPraias[i][1]);                                        
                                    }//FIN CREO ARRAY DE PLAYAS                     
                                                            
                        
                                    
                                    //console.log(objetoConcello);

                                    //Oculto el buscador:
                                    //$("#formconcello").hide();

                                    //Muestro el nombre del concello:
                                    $("#mensaje").html("<h1>Concello de "+nomConcello+"</h1>").addClass("titulo");

                                  

                                    ////////////////////////////////////////////////////////////////////////////////
                                    //ISERTO EL MAPA EN LA PAGINA
                                    //Añadir el mapa:
                                   // const tilesProvider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

                                    //Centrar mapa                        
                                    //var miMapa = L.map('mapa').setView(coordenadasConcello,13);
                                    //var miMapa = L.map('mapa').setView([latitudes[puntoMedio],longitudes[puntoMedio]],13);
                                    /*L.tileLayer(tilesProvider, {
                                        maxZoom:18
                                    }).addTo(miMapa);*/

                                    ////////////////////////////////////////////////////////////////////////////////

                                    //Marcador del concello
                                    var marcadorConcello = L.marker(coordenadasConcello).addTo(miMapa);
                                    miMapa.setView(coordenadasConcello,13);

                                    //Poner las banderas en el mapa ()
                                    var bandera = L.icon({
                                        //iconUrl:"../icons/bandera.png",
                                        iconUrl:"bandera.png",
                                        iconSize: [37,37],
                                        iconAnchor: [5,38],
                                        shadowAnchor: [4,62],
                                        popupAnchor: [4,-40]

                                    })//Fin poner badnera     



                                    

                                    //Consulta de datos extra de praias de concello
                                    $.get("./crud/crud.php",{nombreConcelloBuscarDatosPraias:concello},function(objetoPraias){
                                        //console.log(objetoPraias);

                                        

                                    //console.log(arrayPraias[0][0],arrayPraias[0][1]);
                                    //console.log(arrayPraias);
                                    //Poner las banderas en el mapa
                                    for(var i=0;i<arrayPraias.length-1;i++){
                                        //Texto que tendrá el popup
                                        let texto = "<a href="+objetoPraias[i]["web"]+" target='_blank'>"+objetoPraias[i]["praia"]+"</a><br>";
                                        texto += "<b>Area: </b>"+ objetoPraias[i]["tipoarea"]+"<br>";
                                        texto += "<b>Tamaño: </b>"+objetoPraias[i]["lonxitude"]+" m";
                                        
                                        //Creo los marcadores y el popup para cada bandera
                                        var punto=L.marker([arrayPraias[i][0],arrayPraias[i][1]],{icon:bandera}).addTo(miMapa);
                                        punto.bindPopup(texto);
                                        
                                        //Cada vez que pulso en una bandera:
                                        punto.on("click",function(){
                                            $("#meteogalicia object").attr("data","");
                                            //console.log(this.bindPopup());
                                            //console.log(this["_latlng"]["lat"]);
                                            var latitudPlaya = parseFloat(this["_latlng"]["lat"]);
                                            var longitudPlaya = parseFloat(this["_latlng"]["lng"]);  
                                            //Se le pasa con % para hacer bien la consulta en la base de datos
                                            var coordenadasPlaya =  "%"+latitudPlaya+"%,%"+longitudPlaya+"%";
                                            
                                            //El array que le pasare a la api:
                                            coordenadasPlaya = coordenadasPlaya.trim();
                                            var coordenadasPlayaMeteo = [latitudPlaya.toFixed(7),longitudPlaya.toFixed(7)];
                                            
                                                                                        
                                            //Necesito una consulta para obtener nombre de la playa
                                            $.get("./crud/crud.php",{COORDENADASPLAYA:coordenadasPlaya},function(nombrePraia){
                                                //console.log(nombrePraia);

                                                 //INSERTARE AQUI LO DE METEOGALICIA (ESTA VEZ PARA CADA PLAYA):
                                                $.get("./crud/crud.php",{COORDENADASMETEO:coordenadasPlayaMeteo},function(htmlmeteo1){
                                                    //console.log(htmlmeteo1);
                                                    $("#meteogalicia").show();
                                                    $("#tittiempo").show();
                                                    $("#tittiempo").text("Previsión meteorológica para la playa de "+nombrePraia);

                                                    $("#meteogalicia object").attr("data",htmlmeteo1);

                                                });//FIN API METEOGALICIA


                                            });//Fin obtener nombre playa

                                            //CADA VEZ QUE SE QUIERE CONSULTAR UN TIEMPO
                                            $("#btntiempo").on("click",function(){


                                                //FECHA INICIO BUSQUEDA:
                                                var fechaInicio= $("#fechainicio").val();
                                                var fechaFin= $("#fechafin").val();


                                                //console.log("COORDENADAS: "+coordenadasPlayaMeteo);
                                                //console.log("FECHAINICIO: "+fechaInicio);
                                                //console.log("FECHAFIN: "+fechaFin);

                                                //CONSULTA PARA TIEMPO EN RANGO DE FECHAS EN LAS PLAYAS
                                                $.get("./crud/crud.php",{CORDENADASPLAYAFECHAS:coordenadasPlayaMeteo,FECHAINICIO:fechaInicio,FECHAFIN:fechaFin},function(respuesta){
                                                    //console.log(respuesta);                                                    
                                                    
                                                    if(respuesta=="fechainiciomenordiaactual"){
                                                        $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>La fecha de inicio no puede ser menor que el día actual.</h3>")
                                                    }else if(respuesta=="fechainiciomayorfechafinal"){
                                                        $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>La fecha de inicio no puede ser mayor a la fecha final.</h3>")

                                                    }else if(respuesta=="fueraderango"){
                                                        $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>No hay información meteorológica disponible para el rango indicado.</h3>")
                                                    }
                                                    $("#meteogalicia object").attr("data",respuesta);
                                                });//FIN CONSULTA PARA EL TIEMPO EN RANGO DE FECHAS DE LAS PLAYAS

                                            });//FIN CONSULTA TIEMPO EN RANGO DE DIAS
    

                                        });//Fin cada vez pulsar bandera

                                        //Los añado a marcadoresEliminar para poder eliminarlos
                                        marcadoresEliminar.push(punto);  
                                        
                                        
                                






                                    }//FIN FOR
                                    marcadoresEliminar.push(marcadorConcello);//Añado el marcador del concello para eliminarlo
                                        
                                    


                                    });//Fin consulta datos praias do concello buscado
            
                            }//FIN SI ENCUENTRA PLAYAS                          

                        });//FIN PETICION DE PLAYAS


                        //INSERTARE AQUI LO DE METEOGALICIA:
                        $.get("./crud/crud.php",{COORDENADASMETEO:coordenadasConcello},function(htmlmeteo){
                            //console.log(htmlmeteo);

                            $("#meteogalicia").show();
                            $("#tittiempo").show();
                            $("#tittiempo").text("Previsión meteorológica concello de "+nomConcello);

                            $("#meteogalicia object").attr("data",htmlmeteo);

                        });


                         //CADA VEZ QUE SE QUIERE CONSULTAR UN TIEMPO
                         $("#btntiempo").on("click",function(){


                            //FECHA INICIO BUSQUEDA:
                            var fechaInicio= $("#fechainicio").val();
                            var fechaFin= $("#fechafin").val();


                            //console.log("COORDENADAS: "+coordenadasPlayaMeteo);
                            //console.log("FECHAINICIO: "+fechaInicio);
                            //console.log("FECHAFIN: "+fechaFin);

                            //CONSULTA PARA TIEMPO EN RANGO DE FECHAS EN LAS PLAYAS
                            $.get("./crud/crud.php",{CORDENADASPLAYAFECHAS:coordenadasConcello,FECHAINICIO:fechaInicio,FECHAFIN:fechaFin},function(respuesta){
                                //console.log(respuesta);                                                    
                                
                                if(respuesta=="fechainiciomenordiaactual"){
                                    $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>La fecha de inicio no puede ser menor que el día actual.</h3>")
                                }else if(respuesta=="fechainiciomayorfechafinal"){
                                    $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>La fecha de inicio no puede ser mayor a la fecha final.</h3>")

                                }else if(respuesta=="fueraderango"){
                                    $("#meteogalicia object").html("<h3 style='margin:2cm;color:red;'>No hay información meteorológica disponible para el rango indicado.</h3>")
                                }
                                $("#meteogalicia object").attr("data",respuesta);
                            });//FIN CONSULTA PARA EL TIEMPO EN RANGO DE FECHAS DE LAS PLAYAS

                        });//FIN CONSULTA TIEMPO EN RANGO DE DIAS




                    }//Fin si encuentra el concello

                });//Fin peticion del concello


            }
          
        });




});
