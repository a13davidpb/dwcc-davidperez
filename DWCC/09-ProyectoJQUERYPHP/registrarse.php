<?php
    require 'cabecera.php';

?>

<!--<form action="crud.php?op=registro" method="post">-->
<form id="loginregistro">
        Nombre:<input id="nombre" name="nombre" type="text"><br><br>
        Apellidos:<input id="apellidos" name="apellidos" type="text"><br><br>
        Fecha Nacimiento:<input id="fechanac"  name="fechanac" type="date"><br><br>
        Correo: &nbsp;&nbsp; <input id="usuario" name="usuario" type="email"><br><br>
        Password: <input id="password" name="password"type="password">
        <br><br>
        <!--<a><input id="crear" class="btn" value="Crear Usuario" style="border:0" type="submit"/></a>-->
        <input id="botonacceder"type="button" id="boton" class="btn" value="registrar" style="border:0" name="boton">
</form>
<div style="margin-top:-3.5cm;" id="mensaje"></div>

<script src="js/jquery.js"></script>

<script type="text/javascript">


	$(document).ready(function(){

        //EL BOTON NO DEBE SER SUBMIT
        $("#botonacceder").click(function(event){     
                //Llamo a la funcion
                registrarsePublic();
            });   



        $("input").keyup(function(evento){
            if(evento.which==13){
                //Llamo a la funcion
                registrarsePublic();
            }
        });


        //Funcion que registra los usuarios publicamente.
        function registrarsePublic(){
            var nom = $.trim($("#nombre").val());
            var ape = $.trim($("#apellidos").val());
            var fec = $.trim($("#fechanac").val());
            var ema = $.trim($("#usuario").val());
            var pas = $.trim($("#password").val());

            fechaNacimiento = new Date( $.trim($("#fechanac").val()));
            

            //FUNCION MAYOR DE EDAD
            function mayorEdad(fecha){
                fechanacimiento = new Date(fecha);
                hoy = new Date();
                edad = parseInt((hoy-fechanacimiento)/365/24/60/60/1000);

                if(edad>=18){
                    return true;
                }else{
                    return false;
                }
            }//FIN FUNCION MAYOR DE EDAD


            //Pongo a mayusculas las primeras letras de cada nombre
            var arrayNombre = nom.split(" ");
            for(var i=0;i<arrayNombre.length;i++){
                arrayNombre[i] = arrayNombre[i].charAt(0).toUpperCase()+arrayNombre[i].slice(1);
            }
            nom = arrayNombre.join(" ");
            //Pongo a mayusculas las primeras letras de cada apellido
            var arrayApellido = ape.split(" ");
            for(var i=0;i<arrayApellido.length;i++){
                arrayApellido[i] = arrayApellido[i].charAt(0).toUpperCase()+arrayApellido[i].slice(1);
            }
            ape = arrayApellido.join(" ");


            //LO SIGUIENTE SOLO VALE SI TIENE 1 NOMBRE Y 1 APELLIDO
            //nom = nom.charAt(0).toUpperCase()+nom.slice(1);
            //ape = ape.charAt(0).toUpperCase()+ape.slice(1);

                if( nom == "" || ape == ""|| fec == ""|| ema == ""|| pas == ""){//SI LOS CAMPOS ESTAN VACIOS
                    //Muestro el mensaje:
                    $("#mensaje").addClass("error").text("No pude haber campos vacios.").fadeIn(1000).delay(500).fadeOut(2000);
                }else{//Si no estan vacios, envio al servidor los datos (AL CRUD)



                    //SI ES MAYOR DE EDAD:
                    if(mayorEdad(fechaNacimiento)==true){     
                        //Peticion al crud:        
                        $.post("crud.php",{newnom:nom,newape:ape,newfec:fec,newema:ema,newpas:pas},function(datodevuelto){
                            //console.log(datodevuelto);
                            //Redirecciono:
                            //console.log(datodevuelto);
                            if(datodevuelto=="registrado"){
                                //Vacio los campos del formulario
                                $("#loginregistro")[0].reset();
                                //Imprimo el mensaje
                                $("#mensaje").addClass("correcto").text("Usuario registrado correctamente").fadeIn(1000).delay(500).fadeOut(2000);
                            }else if(datodevuelto=="errorexistente"){
                                $("#usuario").focus();
                                $("#usuario").focus().css("color","red");
                                $("#mensaje").removeClass("correcto");
                                $("#mensaje").addClass("error").text("Usuario existente.").fadeIn(1000).delay(500).fadeOut(2000);
                            }
                        }); 
                    }else if(mayorEdad(fechaNacimiento)==false){
                        $("#fechanac").focus().css("color","red");
                        $("#mensaje").addClass("error").text("Debes de ser mayor de edad").fadeIn(1000).delay(500).fadeOut(2000);
                    }//FIN SI ES MAYOR DE EDAD                              
                }//Fin si no estan vacios los campos            
            

            //Al pulsar en el campo correo y fecha se pone en negro (Por si hay error antes)
            $("#usuario").on("click",function(){
                $(this).css("color","black");
            });
            $("#fechanac").on("click",function(){
                $(this).css("color","black");
            });

        };

	});



</script>

