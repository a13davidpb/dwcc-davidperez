<?php

    require 'cabecera.php';

?>

<!--<form id="formlogin" action="crud.php?op=login" method="post">-->
<form id="formlogin">
        Usuario: &nbsp;&nbsp; <input id="usuario" name="usuario" type="email"><br><br>
        Password: <input id="password" name="password"type="password">
        <br><br>
        <a><!--<input id="botonacceder" class="btn" value="acceder" style="border:0" type="submit"/>-->
        <input id="botonacceder"type="button" id="boton" class="btn"value="acceder" style="border:0" name="boton">
        </a>
</form> 

<form  style="margin-top:-5cm;">
<a href="registrarse.php"><input class="btn" value="registrarse" style="border:0" type="button"/></a>
</form>

<div style="margin-top:-3.5cm;" id="mensaje"></div>
    
<script src="js/jquery.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    //Desactivar cache navegador
    $.ajaxSetup({cache:false});
    //EL BOTON NO DEBE SER SUBMIT
    $("#botonacceder").click(function(event){     
            //Inicio la sesion llamando a la funcion.
            iniciarSesion();
    });
 
    //Tambien accedemos si hacemos click en enter:
    $("input").keyup(function(evento){
        if(evento.which==13){
            //Inicio la sesion llamando a la funcion.
            iniciarSesion();
        }
    });
    

    //Funcion para iniciar sesion (De esta manera no duplico código)
    function iniciarSesion(){
        var usuario = $.trim($("#usuario").val());
        var password = $.trim($("#password").val());

        if( usuario == "" || password == ""){//SI LOS CAMPOS ESTAN VACIOS
            //Muestro el mensaje:
            $("#mensaje").addClass("error").text("No puede haber campos vacios.").fadeIn(1000).delay(500).fadeOut(2000);
        }else{//Si no estan vacios, envio al servidor los datos (AL CRUD)
            //Peticion al crud:        
            $.post("crud.php",{user:usuario,passwd:password},function(datodevuelto){
                //console.log(datodevuelto);
                //Redirecciono:
                if(datodevuelto!="erroriniciosesion"){
                    window.location.replace(datodevuelto);
                }else{
                    $("#mensaje").addClass("error").text("Usuario o contraseña incorrectos").fadeIn(1000).delay(500).fadeOut(2000);
                }
            });           
            
        }
    }


    });


</script>

