<?php
    session_start();  
    
    require "cabeceraadmin.php";	
    
    echo "<br>";
    echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";    

    echo"<br><br>";
    echo '<div id="listado" style="margin:auto;">';
  
    require_once 'conexion.php';

    try{
        $stmt=$pdo->prepare("select * from usuarios where idusuario != 1");//NO PUEDE SALIR EL ADMIN (Peligro, se puede autoeliminar)
        

    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Ejecutamos consulta
        $stmt->execute();

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        
        echo '<table style="margin-left:-2cm;width:700px"  border="1" id="listadousuarios">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Fecha Nacimiento</th>
                    <th>Correo</th>
                    <th>Foto</th>
                    <th>Accion</th>                    
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   
                                 
            echo "<tr>
                    <td> {$fila['idusuario']}</td>
                    <td> {$fila['nombre']}</td>
                    <td> {$fila['apellido']}</td>
                    <td> {$fila['fechanac']}</td>
                    <td> {$fila['correo']}</td>
                    <td> <img style='width:50px' src='{$fila['fotousuario']}'/>
                    <td> 
                        <a class='btneliminar' type='button' value='eliminar' style='border:0' name='boton'>Eliminar</a><br>
                        <a class='btnmodificar' type='button' value='modificar' style='border:0' name='boton'>Modificar</a><br>
                        <a class='btnmodificarfoto' type='button' value='editarfoto' style='border:0' name='boton'>Foto</a>
                    </td>";
                

        };
        $cont++;
        echo '</table>';
    
    } else 
        echo 'No se han encontrado registros de carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";
       
    
    
  ?>

  <body>
  <br>
  <div class="mensaje1" id="mensaje"></div>
  <br>
    
  <form id="loginregistro">
  <strong>CREAR NUEVO USUARIO</strong><br><br>
       <span style="background-color:none;padding-right:60px;">Nombre:</span><input style="width:4cm" id="nombre" name="nombre" type="text"><br>
       <span style="background-color:none;padding-right:53px;margin-top:7px">Apellidos:</span><input style="width:4cm;margin-top:7px" id="apellidos" name="apellidos" type="text"><br>
       <span style="background-color:none;padding-right:6px;margin-top:7px">Fecha Nacimiento:</span><input style="width:4cm;margin-top:7px" id="fechanac"  name="fechanac" type="date"><br>
       <span style="background-color:none;padding-right:65px;margin-top:7px">Correo:</span><input style="width:4cm;margin-top:7px"  id="usuario" name="usuario" type="email"><br>
       <span style="background-color:none;padding-right:40px;margin-top:7px">Contraseña:</span><input style="width:4cm;margin-top:7px"  id="password" name="password"type="password"><br>
        <br>
        <input id="botonCrearUsuario"type="button" id="boton" class="btn"value="crear usuario" style="border:0" name="boton">
</form>


  <script src="js/jquery.js"></script>
  <script type="text/javascript">
	$(document).ready(function(){
        //Desactivar cache navegador
        $.ajaxSetup({cache:false});


        //CADA VEZ QUE SE HACE CLICK EN UN BOTON "eliminar":      
        $("#listadousuarios").on("click",".btneliminar",function(){         
            if(confirm("¿Desea borrar este usuario?")){
                padreTR = $(this).parent().parent();
                //console.log(padreTR.children().first().text());
               
                var idEliminar = padreTR.find("td:eq(0)").text();

                var datosEnviar = {idusuario:idEliminar};
                $.post("crud.php",datosEnviar, function(respuesta){
                    //console.log(respuesta);
                    if(respuesta=="usuarioeliminado"){
                        padreTR.addClass("mal").delay(1000).queue(function (){
                            //Elimino de la pagina la fila
                            $(this).remove();
                            //Recargo pagina
                            location.reload();
                        });
                    }

                });

            };
        });//FIN ELIMINAR USUARIO


       



          //CADA VEZ QUE SE HACE CLICK EN UN BOTON "modificar":      
          $("#listadousuarios").on("click",".btnmodificar",function(){
              //console.log("MODIFICAR");

            	//Si se quiere editar otro registo distinto al que está editando ahora
                    //Evitar dos ediciones a la vez!
                    if($("input.edicion").length!=0){
                        padreTR.html(contenidoOriginalFila);
                    };

                if(confirm('¿Desea editar este usuario?')){	
                    //NECESITAMOS LLEGAR AL "tr"
                    padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
                    //Obtenemos el nombre del pais de la fila
                    idOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del usuario							
                    //Contenido original de la fila:(Se necesitara en un futuro cuando se seleccione otra fila)
                    contenidoOriginalFila=padreTR.html();

                    //Recorremos los td y convertimos el texto en cajas de texto
                    padreTR.find("td").slice(1,5).each(function(){
                        //Creamos cajas de texto en cada campo de la fila
                        $(this).html("<input style='width:2cm;' type='text' class='edicion' value='"+$(this).text().trim()+"'/>");//Con esto podremos editar la fila 
                    });

                    //Al pulsar Enter en alguna de las cajas de texto... realiza la consulta de edicion
                    $("input").keyup(function(evento){
                        //el numero 13 es el codigo del "INTRO"
                        /*console.log($("input:eq(0)").val());
                        console.log($("input:eq(1)").val());
                        console.log($("input:eq(2)").val());
                        console.log($("input:eq(3)").val());*/

                        if(evento.which==13){//Si pulsamos ENTER hacemos la peticion ajax...
                            var datosEnviar={
                                /*idusuariomod:$("input:eq(0)").val().trim(),*/
                                nombreusuariomod:$("input:eq(0)").val(),
                                apellidosusuariomod:$("input:eq(1)").val(),
                                fechausuariomod:$("input:eq(2)").val(),
                                correousuariomod:$("input:eq(3)").val(),
                                idOriginal:idOriginal                                
                            };
                            //console.log(datosEnviar);

                            //Hacemos la consulta a la base de datos
                            $.post("crud.php",datosEnviar,function(respuesta){
                               //console.log(respuesta);
                               
                                //Salir de la edicion y mostrar durante 1 segundo la fila editada en color verde:
                                if(respuesta == "usuariomodificado"){
                                    //Sacamos los input y dejamos las celdas
                                    padreTR.find("td").slice(1,5).each(function(){
                                        $(this).text($(this).find("input").val());
                                    });
                                    //Poner fondo VERDE durante un segundo
                                    padreTR.addClass('bien').delay(1000).queue(function(){
                                        $(this).removeClass('bien');
                                        location.reload();
                                    });//Fin BIEN
                                }else if(respuesta == "errormodificarusuario"){
                                    padreTR.addClass('mal').delay(1000).queue(function(){
                                        $(this).removeClass('bien');
                                    });//Fin MAL
                                }else if(respuesta =="erroradmin"){
                                    $("#mensaje").html("<strong style='color:#5c1521;'>ERROR: No se puede añadir, cuenta de correo existente.</strong>").fadeIn(5).delay(500).fadeOut(1000);
                                }
                            });//Fin POST edicion de datos  
                        };
                    });//Fin pulsar enter edicion de datos
                      
                };

          });


          //CREAR UN NUEVO USUARIO (Copiado de registrarse.php)
           //EL BOTON NO DEBE SER SUBMIT
        $("#botonCrearUsuario").click(function(event){
                //Llamo a la funcion
                crearUsuario() ;
        });   
            //Crear el usuario dandole a intro:
        $("input").keyup(function(evento){
            if(evento.which==13){
                //Llamo a la funcion
                crearUsuario();
            }
        });


            //Creo una funcion para no duplicar codigo:
            function crearUsuario(){
                    var nom = $.trim($("#nombre").val());
                    var ape = $.trim($("#apellidos").val());
                    var fec = $.trim($("#fechanac").val());
                    var ema = $.trim($("#usuario").val());
                    var pas = $.trim($("#password").val());


                    //EL USUARIO DEBE SER MAYOR DE EDAD
                    fechaNacimiento = new Date( $.trim($("#fechanac").val()));
                    //FUNCION MAYOR DE EDAD
                    function mayorEdad(fecha){
                        fechanacimiento = new Date(fecha);
                        hoy = new Date();
                        edad = parseInt((hoy-fechanacimiento)/365/24/60/60/1000);

                        if(edad>=18){
                            return true;
                        }else{
                            return false;
                        }
                    }//FIN FUNCION MAYOR DE EDAD

                    
                    //Pongo a mayusculas las primeras letras de cada nombre
                    var arrayNombre = nom.split(" ");
                    for(var i=0;i<arrayNombre.length;i++){
                        arrayNombre[i] = arrayNombre[i].charAt(0).toUpperCase()+arrayNombre[i].slice(1);
                    }
                    nom = arrayNombre.join(" ");
                    //Pongo a mayusculas las primeras letras de cada apellido
                    var arrayApellido = ape.split(" ");
                    for(var i=0;i<arrayApellido.length;i++){
                        arrayApellido[i] = arrayApellido[i].charAt(0).toUpperCase()+arrayApellido[i].slice(1);
                    }
                    ape = arrayApellido.join(" ");



                        if( nom == "" || ape == ""|| fec == ""|| ema == ""|| pas == ""){//SI LOS CAMPOS ESTAN VACIOS
                            //Muestro el mensaje:
                            $("#mensaje").addClass("error").text("No pude haber campos Vacios.").fadeIn(1000).delay(500).fadeOut(2000);
                        }else{//Si no estan vacios, envio al servidor los datos (AL CRUD)

                            //SI ES MAYOR DE EDAD:
                            if(mayorEdad(fechaNacimiento)==true){

                                    //Peticion al crud:        
                                    $.post("crud.php",{newnom:nom,newape:ape,newfec:fec,newema:ema,newpas:pas},function(datodevuelto){
                                        //console.log(datodevuelto);
                                        //Redirecciono:
                                        //console.log(datodevuelto);
                                        if(datodevuelto=="registrado"){
                                            /*$("#mensaje").addClass("correcto").text("Usuario registrado correctamente").fadeIn(1000).delay(500).fadeOut(2000);*/
                                            location.reload();
                                        }else if(datodevuelto=="errorexistente"){
                                            $("#mensaje").removeClass("correcto");
                                            $("#mensaje").addClass("error").text("Usuario existente.").fadeIn(1000).delay(500).fadeOut(2000);
                                            
                                        }
                                    }); 
                            }else if(mayorEdad(fechaNacimiento)==false){
                                    $("#mensaje").addClass("error").text("El usuario debe ser mayor de edad.").fadeIn(1000).delay(500).fadeOut(2000);
                            }
                        }
                }

             //Desactivar cache navegador
        $.ajaxSetup({cache:false});

//MODIFICAR FOTO : CADA VEZ QUE SE HACE CLICK EN UN BOTON "Editar foto":
$("#listadousuarios").on("click",".btnmodificarfoto",function(){

      //Si se quiere editar otro registo distinto al que está editando ahora
    //Evitar dos ediciones a la vez!
    if($("input.edicion").length!=0){
        padreTR.html(contenidoOriginalFila);
    };
                
    if(confirm('¿Desea cambiar la foto de este usuario?')){	

        

        //NECESITAMOS LLEGAR AL "tr"
        padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
        idOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del usuario
        contenidoOriginalFila=padreTR.html();

        //Hacemos aparecer el input file
        // <br> <input type='button' value='ok' name='ok/>
        padreTR.find("td").eq(5).html("<input class='edicion'type='file' name='fotousuariomod' accept='image/*'/><input id='guardar' type='button' name='guardar' value='guardar'/>");

        $("#guardar").on('click',function(){//AL DARLE CLICK AL BOTON GUARDAR                        
                /*console.log($("input:eq(0)"));*/

                //console.log($("input[type=file]")[0].files[0]);
                //console.log($("input[type=file]")[0].files[0]['name']); //->NOMBRE DE LA FOTO
                let datosEnviar = new FormData();
                if($("input[type=file]")[0].files[0]==undefined){//Si no se introduce ninguna imagen                           


                    nombredefecto = "./imagenes/default.jpg";
                    datosEnviar.append('nombreFoto',nombredefecto);
                    datosEnviar.append('idusuarioeliminarfoto',idOriginal);


                    //$("#mensaje").html("<strong style='color:#5c1521;'>ERROR: Introduce una imagen.</strong>").fadeIn(5).delay(500).fadeOut(1000);
                }else{
                    
                    //LE ENVIO EL NOMBRE DEL ARCHIVO (PARA UTILIZAR isset)
                    datosEnviar.append('nombreFoto',$("input[type=file]")[0].files[0]["name"]);
                    //LE ENVIO EL ARCHIVO
                    datosEnviar.append('fotousuariomod',$("input[type=file]")[0].files[0]);
                    //LE ENVIO EL ID
                    datosEnviar.append('idusuarioeliminarfoto',idOriginal)
                }

                    $.ajax({
                        url:'crud.php',
                        dataType: 'text',
                        data: datosEnviar,
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        success: function(r){
                            successAct(r);
                        },
                        error: function(respuesta){
                            console.log("ERROR AJAX");
                        }
                    });
                
        });

        function successAct(respuesta){
                //console.log(respuesta);
                location.reload();
        }




    };
    
});//FIN EDITARFOTO













    });
  </script>

  </body>