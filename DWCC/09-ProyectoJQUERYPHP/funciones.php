<?php
/**
* Función que encripta una password utilizando SHA-512 y 10000 vueltas por defecto
*
* @param string $password
* @param int $vueltas Número de vueltas, opcional. Por defecto 10000.
* @return string
*/ 
function encriptar($password, $vueltas = 10000) {
// Empleamos SHA-512 con 10000 vueltas por defecto.

    $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);

    return crypt((string) $password, '$6$' . "rounds=$vueltas\$$salt\$");

// Forma de comprobación de contraseña en login:
// Es recomendable comparar las cadenas de hash con hash_equals que es una comparación segura contra ataques de timing.

// Ejemplo de uso (hash_equals para evitar ataques de Timing)

// if (hash_equals(crypt($passRecibidaFormulario, $passwordAlmacenadaEncriptada),$passwordAlmacenadaEncriptada))

// Es necesario pasarle a la función crypt la $passwordAlmacenadaEncriptada para que pueda extraer la semilla, el tipo de encriptación y el número de vueltas.
}

 
/**
 * @param array Se le pasa un array y lo devuelve filtrado.

 */
function limpiarFiltrar($arrayFormulario)
{
    foreach ($arrayFormulario as $campo => $valor)
    {
        $arrayFormulario[$campo]=htmlspecialchars(trim($arrayFormulario[$campo]));
    }

    return $arrayFormulario;
}


/**
 * Función que comprueba los campos obligatorios de un formulario y los filtra.
 * @param array [Array con los campos que se consideran obligatorios. Ejemplo: array('nombre','apellidos','dni') ]
 * @param metodoRecepcion   post|get
 */

function camposObligatoriosOK($arrayCamposObligatorios,$metodoRecepcion)
{
    $datosValidados = array();

    if (strtoupper($metodoRecepcion)=='POST')
        $arrayRecibidos=&$_POST;
    else
        $arrayRecibidos=&$_GET;

    for($i=0; $i<count($arrayCamposObligatorios); $i++)
    {
        $campo=$arrayCamposObligatorios[$i];

        if (!isset($arrayRecibidos[$campo]))
            return false;

        if (isset($arrayRecibidos[$campo]) && $arrayRecibidos[$campo]=='')
            return false;
    }

    return true;
}

/**
 * Almacena mensaje flash en variable de sesión.
 * @param  [string] $textoMensaje [description]
 * @param  [string] $tipoMensaje  [los de bootstrap, 'warning', 'danger', 'success',...]
 */
function mensajeFlash($textoMensaje,$tipoMensaje)
{
    $_SESSION['mensajeflash']=$textoMensaje;
    $_SESSION['tipoflash']=$tipoMensaje;
}



/**
 * [valorErroneo description]
 * @param  string $campo nombre del campo a comprobar en SESSION['errores']
 * @return [string]       Devuelve el valor del campo almacenado en la sesión.
 */
function valorErroneo($campo)
{
    if (isset($_SESSION['errores'][$campo]) && $_SESSION['errores'][$campo]!='')
        return $_SESSION['errores'][$campo];
    else
        return '';
}

function subirImagen() {
        $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
        $fileName = $_FILES['uploadedFile']['name'];
        $fileSize = $_FILES['uploadedFile']['size'];
        $fileType = $_FILES['uploadedFile']['type'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

        $allowedfileExtensions = array('jpg', 'gif', 'png', 'jpeg');

        if (in_array($fileExtension, $allowedfileExtensions)) {
            $uploadFileDir = 'imagenes/';
            $dest_path = $uploadFileDir . $newFileName;
             
            if(move_uploaded_file($fileTmpPath, $dest_path))
            {
              $message ='Imagen subida.';
            }
            else
            {
              $message = 'Error en la subida de la imagen al servidor.';
            }
        }
        return $newFileName;
}

