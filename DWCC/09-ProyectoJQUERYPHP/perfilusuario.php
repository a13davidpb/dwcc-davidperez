<?php
    session_start();  
    
    require "cabecerausuario.php";	
    
    echo "<br>";
    echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";    

    echo"<br><br>";
    echo '<div id="listado" style="margin:auto;">';
  
    require_once 'conexion.php';

    $correoUsuarioLogeado = $_SESSION["usuario"];

    $consultaID = $pdo->query("select idusuario from usuarios where correo='$correoUsuarioLogeado'");
    $consultaID->execute();
    while($row = $consultaID->fetch()){
        $idUsuarioLogeado = $row['idusuario'];			
    }


    try{
        $stmt=$pdo->prepare("select * from usuarios where idusuario = $idUsuarioLogeado");//NO PUEDE SALIR EL ADMIN (Peligro, se puede autoeliminar)
        

    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Ejecutamos consulta
        $stmt->execute();

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        
        echo '<table style="margin-left:-2cm;width:700px"  border="1" id="listadousuarios">
                <tr>
                    <th style="display:none">ID</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Fecha Nacimiento</th>
                    <th>Correo</th>
                    <th>Foto</th>
                    <th>Accion</th>                    
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   
                                 
            echo "<tr>
                    <td style='display:none'> {$fila['idusuario']}</td>
                    <td> {$fila['nombre']}</td>
                    <td> {$fila['apellido']}</td>
                    <td> {$fila['fechanac']}</td>
                    <td> {$fila['correo']}</td>
                    <td> <img style='width:50px' src='{$fila['fotousuario']}'/>
                    <td > 
                        <a class='btneliminar' type='button' value='eliminar' style='border:0' name='boton'>Cuenta</a><br>
                        <a class='btnmodificar' type='button' value='modificar' style='border:0' name='boton'>Modificar</a><br>
                        <a class='btnmodificarfoto' type='button' value='editarfoto' style='border:0' name='boton'>Foto</a>
                    </td>";
                

        };
        $cont++;
        echo '</table>';
    
    } else 
        echo 'No se han encontrado registros de carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";
       
    
    
  ?>

  <body>
  <br>
  <div id="mensaje"></div>
  <br>


  <script src="js/jquery.js"></script>
  <script type="text/javascript">
	$(document).ready(function(){
        //Desactivar cache navegador
        $.ajaxSetup({cache:false});


        //CADA VEZ QUE SE HACE CLICK EN UN BOTON "eliminar":      
        $("#listadousuarios").on("click",".btneliminar",function(){         
            if(confirm("¿Deseas darte de baja?")){
                padreTR = $(this).parent().parent();
                //console.log(padreTR.children().first().text());
               
                var idEliminar = padreTR.find("td:eq(0)").text();

                var datosEnviar = {idusuario:idEliminar};
                $.post("crud.php",datosEnviar, function(respuesta){
                    //console.log(respuesta);
                    if(respuesta=="usuarioeliminado"){
                        padreTR.addClass("mal").delay(1000).queue(function (){
                            //Elimino de la pagina la fila
                            $(this).remove();
                            
                            //Recargo pagina
                            window.location.href="index.php";
                        });
                    }

                });

            };
        });//FIN ELIMINAR USUARIO     



          //CADA VEZ QUE SE HACE CLICK EN UN BOTON "modificar":      
          $("#listadousuarios").on("click",".btnmodificar",function(){
              //console.log("MODIFICAR");

            	//Si se quiere editar otro registo distinto al que está editando ahora
                    //Evitar dos ediciones a la vez!
                    if($("input.edicion").length!=0){
                        padreTR.html(contenidoOriginalFila);
                    };

               // if(confirm('¿Desea editar este usuario?')){	
                    //NECESITAMOS LLEGAR AL "tr"
                    padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
                    //Obtenemos el nombre del pais de la fila
                    idOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del usuario							
                    //Contenido original de la fila:(Se necesitara en un futuro cuando se seleccione otra fila)
                    contenidoOriginalFila=padreTR.html();

                    //Recorremos los td y convertimos el texto en cajas de texto
                    padreTR.find("td").slice(1,3).each(function(){
                        //Creamos cajas de texto en cada campo de la fila
                        $(this).html("<input style='width:2cm;' type='text' class='edicion' value='"+$(this).text().trim()+"'/>");//Con esto podremos editar la fila 
                    });

                    //Al pulsar Enter en alguna de las cajas de texto... realiza la consulta de edicion
                    $("input").keyup(function(evento){
                        //el numero 13 es el codigo del "INTRO"
                        /*console.log($("input:eq(0)").val());
                        console.log($("input:eq(1)").val());
                        console.log($("input:eq(2)").val());
                        console.log($("input:eq(3)").val());*/

                        if(evento.which==13){//Si pulsamos ENTER hacemos la peticion ajax...
                        
                            var datosEnviar={
                                /*idusuariomod:$("input:eq(0)").val().trim(),*/
                                nombreusuariomod:$("input:eq(0)").val(),
                                apellidosusuariomod:$("input:eq(1)").val(),
                                fechausuariomod:$("input:eq(2)").val(),
                                /*correousuariomod:$("input:eq(3)").val(),*/
                                idOriginal:idOriginal                                
                            };
                            //console.log(datosEnviar);

                            //Hacemos la consulta a la base de datos
                            $.post("crud.php",datosEnviar,function(respuesta){
                               console.log(respuesta);
                               
                                //Salir de la edicion y mostrar durante 1 segundo la fila editada en color verde:
                                if(respuesta == "usuariomodificado"){
                                    //Sacamos los input y dejamos las celdas
                                    padreTR.find("td").slice(1,5).each(function(){
                                        $(this).text($(this).find("input").val());
                                    });
                                    //Poner fondo VERDE durante un segundo
                                    padreTR.addClass('bien').delay(1000).queue(function(){
                                        $(this).removeClass('bien');
                                        location.reload();
                                    });//Fin BIEN
                                }else if(respuesta == "errormodificarusuario"){
                                    padreTR.addClass('mal').delay(1000).queue(function(){
                                        $(this).removeClass('bien');
                                    });//Fin MAL
                                }else if(respuesta =="erroradmin"){
                                    $("#mensaje").html("<strong style='color:#5c1521;'>ERROR: No se puede añadir, cuenta de correo existente.</strong>").fadeIn(5).delay(500).fadeOut(1000);
                                }
                            });//Fin POST edicion de datos  
                        };
                    });//Fin pulsar enter edicion de datos
                      
                //};

          });


//MODIFICAR FOTO : CADA VEZ QUE SE HACE CLICK EN UN BOTON "Editar foto":
$("#listadousuarios").on("click",".btnmodificarfoto",function(){

//Si se quiere editar otro registo distinto al que está editando ahora
//Evitar dos ediciones a la vez!
if($("input.edicion").length!=0){
  padreTR.html(contenidoOriginalFila);
};
          
if(confirm('¿Desea cambiar la foto de este usuario?')){	

  

  //NECESITAMOS LLEGAR AL "tr"
  padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
  idOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del usuario
  contenidoOriginalFila=padreTR.html();

  //Hacemos aparecer el input file
  // <br> <input type='button' value='ok' name='ok/>
  padreTR.find("td").eq(5).html("<input type='file' name='fotousuariomod' accept='image/*'/><input id='guardar' type='button' name='guardar' value='guardar'/>");

  $("#guardar").on('click',function(){//AL DARLE CLICK AL BOTON GUARDAR                        
          /*console.log($("input:eq(0)"));*/

          //console.log($("input[type=file]")[0].files[0]);
          //console.log($("input[type=file]")[0].files[0]['name']); //->NOMBRE DE LA FOTO
          let datosEnviar = new FormData();
          if($("input[type=file]")[0].files[0]==undefined){//Si no se introduce ninguna imagen                           


              nombredefecto = "./imagenes/default.jpg";
              datosEnviar.append('nombreFoto',nombredefecto);
              datosEnviar.append('idusuarioeliminarfoto',idOriginal);


              //$("#mensaje").html("<strong style='color:#5c1521;'>ERROR: Introduce una imagen.</strong>").fadeIn(5).delay(500).fadeOut(1000);
          }else{
              
              //LE ENVIO EL NOMBRE DEL ARCHIVO (PARA UTILIZAR isset)
              datosEnviar.append('nombreFoto',$("input[type=file]")[0].files[0]["name"]);
              //LE ENVIO EL ARCHIVO
              datosEnviar.append('fotousuariomod',$("input[type=file]")[0].files[0]);
              //LE ENVIO EL ID
              datosEnviar.append('idusuarioeliminarfoto',idOriginal)
          }

              $.ajax({
                  url:'crud.php',
                  dataType: 'text',
                  data: datosEnviar,
                  type: 'POST',
                  contentType: false,
                  processData: false,
                  success: function(r){
                      successAct(r);
                  },
                  error: function(respuesta){
                      console.log("ERROR AJAX");
                  }
              });
          
  });

  function successAct(respuesta){
          console.log(respuesta);
          location.reload();
  }




};

});//FIN EDITARFOTO

    });
  </script>

  </body>