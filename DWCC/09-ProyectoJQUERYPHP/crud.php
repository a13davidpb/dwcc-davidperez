<?php

require_once 'funciones.php';
require 'conexion.php';



//LOGIN:
//if (!empty($_POST['user']&&!empty($_POST["passwd"])))//NO ME GUSTA UTILIZAR EL !empty porque limita el uso del crud.

if(isset($_POST["user"])&&isset($_POST["passwd"]))
{
	
	$cuentausuario = $_POST["user"];
	$passwordEncriptada = md5($_POST["passwd"]);//Esto es la password introducida
  
	//REALIZAR LA CONSULTA A LA BASE DE DATOS
	//Compruebo si el correo existe ya en la base de datos
	$passwordBD="";
	try{
		$consultaCorreos=$pdo->prepare("select password from usuarios where correo='$cuentausuario'");
		$consultaCorreos->execute();                   
		
		while($fila = $consultaCorreos->fetch()){                            
				$passwordBD=$fila[0];//Esto es la contraseña encriptada de la BD.
		}

		//echo $passwordBD;
		//echo print_r($arrayCorreos);

	}catch(PDOException $e){
		//echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
	}

	if($passwordEncriptada==$passwordBD){
		//echo$passwordEncriptada;
		//INICIO LA SESION.
		session_start();
		$_SESSION["usuario"]=$cuentausuario;

		//SI ERES ADMINISTRADOR:
		if($_SESSION["usuario"]=="admin@admin.com"){
			//header("location:indexadmin.php");
			echo("indexadmin.php");
		}else{
			//SI ERES CUALQUIER OTRO USUARIO
			echo("indexusuario.php");
		}   
	}else{
		//ERROR DE INICIO DE SESION		
		//header("location:login.php?access=no");
		echo "erroriniciosesion";

	}
}//FIN LOGIN


//REGISTRO
//newnom:mom,newape:ape,newfec:fec,newema:ema,newpas:pas
if(isset($_POST["newnom"])&&isset($_POST["newape"])&&isset($_POST["newfec"])&&isset($_POST["newema"])&&isset($_POST["newpas"])){
	//echo "ok";
	$nombre = $_POST["newnom"];
	$apellidos = $_POST["newape"];
	$fechanac = $_POST["newfec"];
	$correo = $_POST["newema"];
	$passwordEncriptada = md5($_POST["newpas"]);
	$imagenDefault = "imagenes/default.jpg";
	

	//Compruebo si el correo existe ya en la base de datos
	$arrayCorreos=[];
	try{
		$consultaCorreos=$pdo->prepare("select correo from usuarios");
		$consultaCorreos->execute();                   
		
		while($fila = $consultaCorreos->fetch()){
				array_push($arrayCorreos,$fila[0]);
		}

		//echo print_r($arrayCorreos);

	}catch(PDOException $e){
		echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
	}


	if(in_array($correo,$arrayCorreos)){
		//YA EXISTE ESE CORREO
		//header("location:index.php?creado=error");
		//echo '{"status":"error correo existente"}';
		//header("location:registrarse.php?creado=no");
		echo"errorexistente";
	}else{


		/*INSERTO EN LA BASE DE DATOS*/
		try{
			$stmt=$pdo->prepare("insert into usuarios(nombre,apellido,fechanac,correo,password,fotousuario) values (?,?,?,?,?,?)");

			$stmt->bindParam(1,$nombre);
			$stmt->bindParam(2,$apellidos);
			$stmt->bindParam(3,$fechanac);
			$stmt->bindParam(4,$correo);
			$stmt->bindParam(5,$passwordEncriptada);
			$stmt->bindParam(6,$imagenDefault);
			$stmt->execute();

			
			if($stmt->rowCount()!=0){
			// echo '{"status":"okdavid"}';
			}else{
				//echo '{"status":"error"}';
			}
		
		//header("location:index.php?creado=ok");
			//echo '{"status":"usuariocreado"}';
			//header("location:registrarse.php?creado=ok");
			echo("registrado");




		}catch(PDOException $e){
			echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
		}
	}
}

//ELIMINAR USUARIO (listadousuariosadmin.php)
if(isset($_POST["idusuario"])){ //TENGO QUE NEGAR LA SUBIDA DE LA FOTO, SI NO CUANDO SUBO UNA FOTO Y LE DOY A GUARDAr, COMO TAMBIEN UTILIZO EL ID, SE BORRA.
	//echo $_POST["idusuario"]; //ID DEL USUARIO

	try{
		$consultaCorreos=$pdo->prepare("delete from usuarios where idusuario=".$_POST["idusuario"]);
		$consultaCorreos->execute();                   
		
		echo "usuarioeliminado";

	}catch(PDOException $e){
		//echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
	}
}//FIN ELIMINAR USUARIO

//MODIFICAR USUARIO (listadousuariosadmin.php)
/*idusuariomod,nombreusuariomod,apellidosusuariomod,fechausuariomod,correousuariomod*/
if(isset($_POST["nombreusuariomod"])&&isset($_POST["apellidosusuariomod"])&&isset($_POST["fechausuariomod"])&&isset($_POST["correousuariomod"])){
	

	/*$id=$_POST["idusuariomod"];*/
	$nombre = $_POST["nombreusuariomod"];
	$apellidos = $_POST["apellidosusuariomod"];
	$fecha =$_POST["fechausuariomod"];
	$correo = $_POST["correousuariomod"];
	$idOriginal = $_POST["idOriginal"];
	//echo $id." ".$nombre." ".$apellidos." ".$fecha." ".$correo;
	//echo $idOriginal;
	

	$nombre = ucwords($nombre);
	$apellidos = ucwords($apellidos);


	//COMPROBAR SI EXISTE ALGUN CORREO IGUAL AL INTRODUCIDO
	$arrayusuarios=[];
	$existe=0;	
	try{
		$consulta = $pdo->query("select * from usuarios where correo='$correo'");		
		while($row = $consulta->fetch()){
			array_push($arrayusuarios,$row);
		}
		$existe=count($arrayusuarios);			
	}catch(PDOException $e){
		
	}

	//Obtener el correo actual del usuario a modificar 
	//(Esto se utilizara en las comprobaciones, ya que si el correo del usuario actual es igual 
	//al mismo correo del usuario actual, no deberia de dar error, pero si es igual al correo de otro usuario si)
		$consulta1 = $pdo->query("select correo from usuarios where idusuario='$idOriginal'");
		while($row = $consulta1->fetch()){
			$correoActual = $row['correo'];			
		}

	//Si el correo introducido es "admin@admin.com":
	if($correo=="admin@admin.com"){
		echo "erroradmin";
	}else if($existe==0||$existe==1&&$correo==$correoActual) {//Si no es "admin@admin.com y no existe en la BD:
		try{
			$stmt=$pdo->prepare("update usuarios set nombre='$nombre', apellido='$apellidos',fechanac='$fecha',correo='$correo' where idusuario='$idOriginal'");
			$stmt->execute();
			echo "usuariomodificado";
		}catch(PDOException $e){
			echo "errormodificarusuario";
		}
	}else if($existe==1 && $correo!==$correoActual){
		echo "erroradmin";
	}

}//FIN MODIFICAR USUARIO COMO ADMINISTRADOR


//USUARIO MODIFICANDOSE A SI MISMO

//if(isset($_POST["nombreusuariomod"])&&isset($_POST["apellidosusuariomod"])&&isset($_POST["fechausuariomod"])){
if(isset($_POST["nombreusuariomod"])&&isset($_POST["apellidosusuariomod"])){
	$nombre = $_POST["nombreusuariomod"];
	$apellidos = $_POST["apellidosusuariomod"];
	//$fecha =$_POST["fechausuariomod"];
	$idOriginal = $_POST["idOriginal"];

	$nombre = ucwords($nombre);
	$apellidos = ucwords($apellidos);


	
	try{
		//$stmt=$pdo->prepare("update usuarios set nombre='$nombre', apellido='$apellidos',fechanac='$fecha' where idusuario='$idOriginal'");
		$stmt=$pdo->prepare("update usuarios set nombre='$nombre', apellido='$apellidos' where idusuario='$idOriginal'");
		$stmt->execute();
		echo "usuariomodificado";
	}catch(PDOException $e){
		echo "errormodificarusuario";
	}


};//FIN USUARIO MODIFICANDOSE



//MODIFICAR FOTO DEL USUARIO:
if(isset($_POST["nombreFoto"])&&isset($_POST["idusuarioeliminarfoto"])){
	//NO SE PUEDE HACER UN ECHO DE LA FOTO ["fotousuariomod"]

	$nombreFoto = $_POST["nombreFoto"];
	$idUsuarioActual = $_POST["idusuarioeliminarfoto"];

	echo $nombreFoto;

	if($_POST["nombreFoto"]=="./imagenes/default.jpg"){

		$cambiar = $pdo->prepare("update usuarios set fotousuario='$nombreFoto' where idusuario='$idUsuarioActual'");

		$cambiar->execute();
		



	}else{
		$stmt = $pdo->prepare("update usuarios set fotousuario=:fotousuariomod where idusuario=:idOriginal");
		/*$stmt->bindValue(':fotousuariomod',$_POST['fotousuariomod']);*/
		$stmt->bindValue(':idOriginal',$_POST['idusuarioeliminarfoto']);

		//Para el archivo de imagen de usuario:
		$fileTmpPath = $_FILES["fotousuariomod"]["tmp_name"];
		$fileName = $_FILES["fotousuariomod"]['name'];
		$fileSize = $_FILES["fotousuariomod"]['size'];
		$fileType = $_FILES["fotousuariomod"]['type'];
		$fileNameCmps = explode(".", $fileName);
		$fileExtension = strtolower(end($fileNameCmps));
		$newFileName = md5(time() . $fileName) . '.' . $fileExtension;

		$extensionspermitidas = array('jpg', 'gif', 'png');

		if (in_array($fileExtension, $extensionspermitidas)){
				//Guardar archivo en el directorio
				
				$uploadFileDir = './imagenes/';
				$dest_path = $uploadFileDir . $newFileName;
				move_uploaded_file($fileTmpPath, $dest_path);
				//Guardar en la BD
				$stmt->bindValue(':fotousuariomod', $dest_path);
		}else{
			echo "ERROR AL SUBIR EL ARCHIVO";
		}

		$stmt->execute();
		echo "FOTO SUBIDA";


	}

};//FIN MODIFICAR FOTO

//SUGERIR CARRERAS (buscadorcarrerasadmin.php)
if(isset($_GET['nombreCarreraSugerir'])){
	//echo $_GET['nombreCarrera'];
	$stmt=$pdo->prepare("select nombre from carreras where nombre like :busqueda");
	$stmt->bindValue(':busqueda','%'.$_GET['nombreCarreraSugerir'].'%');
	$stmt->execute();

	while($fila=$stmt->fetch()){
		echo "<li >".$fila['nombre']."</li>\n";

	}
}//FIN SUGERENCIA CARRERAS


//BUSQUEDA DE PAISES (buscadorcarrerasadmin.php)
if(isset($_GET["nombreCarreraBuscar"])){
	header('Content-type: application/json');
	
	/*echo $_GET["nombreCarreraBuscar"];*/

	$stmt=$pdo->prepare("select * from carreras where nombre=:nombreCarrera");
	$stmt->bindParam(':nombreCarrera',$_GET["nombreCarreraBuscar"]);

	$stmt->execute();

	if($stmt->rowCount()!=0){
		echo json_encode($stmt->fetch());		
	}else{
		/*echo "carreranoencontrada";*/
		echo '{"status":"carreranoencontrada"}';
	}
}//FIN BUSQUEDA


//APUNTARME A CARRERA (buscadorcarrerasusuario.php)
if(isset($_GET["idCarreraApuntarme"])){
	session_start();
	/*echo $_GET["idCarreraApuntarme"];*/
	/*echo $_SESSION["usuario"];*/

	$correoUsuarioApuntarse=$_SESSION["usuario"];
	$idCarreraApuntarUsuario = $_GET["idCarreraApuntarme"];

	/*echo $idCarreraApuntarUsuario;*/

	//TENGO QUE OBTENER EL ID DEL USUARIO BUSCANDO POR CORREO
	$consulta1 = $pdo->query("select idusuario from usuarios where correo='$correoUsuarioApuntarse'");
	while($row = $consulta1->fetch()){
		$idUsuarioApuntarse = $row['idusuario'];			
	}
	

	//COMPROBAR SI YA ESTOY APUNTADO (SI YA ESTOY APUNTADO NO PUEDO VOLVER A APUNTARME)

	$apuntado = $pdo->query("select idinscripcion from inscripciones where idusuarioinscripcion='$idUsuarioApuntarse' and idcarrerainscripcion='$idCarreraApuntarUsuario'");
	$apuntado->execute();
	

	$arrayVecesInscrito=[];

	while($fila=$apuntado->fetch()){
		
		array_push($arrayVecesInscrito,$fila);
	}

	$vecesInscrito=count($arrayVecesInscrito);
		if($vecesInscrito==0){
			//EN ESTE PUNTO TENGO EL ID DEL USUARIO Y EL ID DE LA CARRERA A APUNTARSE
			try{$inscribir = $pdo->prepare("insert into inscripciones (idinscripcion,idusuarioinscripcion,idcarrerainscripcion)values(NULL,$idUsuarioApuntarse,$idCarreraApuntarUsuario)");
				$inscribir->execute();
				echo "inscritoencarrera";
			}catch(PDOException $e){
				echo "errorinscribirse";
			}
		}else{
			echo "errorinscribirse";
		}
}//FIN APUNTARSE A CARRERA

//DESAPUNTARME A CARRERA (buscadorcarrerasusuario.php)
if(isset($_GET["idCarreraDesapuntarme"])){
	session_start();
	$correoUsuarioDesapuntarse=$_SESSION["usuario"];
	$idCarreraDesapuntarUsuario = $_GET["idCarreraDesapuntarme"];

	
	//TENGO QUE OBTENER EL ID DEL USUARIO BUSCANDO POR CORREO
	$consulta1 = $pdo->query("select idusuario from usuarios where correo='$correoUsuarioDesapuntarse'");
	while($row = $consulta1->fetch()){
		$idUsuarioDesapuntarse = $row['idusuario'];			
	}
	try{
		$desapuntar=$pdo->prepare("delete from inscripciones where idusuarioinscripcion='$idUsuarioDesapuntarse' and idcarrerainscripcion='$idCarreraDesapuntarUsuario'");
		$desapuntar->execute();
		echo "desapuntadocarrera";
	}catch(PDOException $e){
		echo "errordesapuntarse";
	}

}//FIN DESAPUNTARSE A CARRERA


//CREAR CARRERA (buscadorcarrerasadmin.com);
//newnomcar:nombreNuevaCarrera,newlugcar:lugarNuevaCarrera,newfeccar:fechaNuevaCarrera

if(isset($_POST['newnomcar'])&&isset($_POST['newlugcar'])&&isset($_POST['newfeccar'])){

		$nombreCarrera = strtolower($_POST['newnomcar']);
		$lugarCarrera = strtolower($_POST['newlugcar']);
		$fechaCarrera = $_POST['newfeccar'];

		//Compruebo si existe en la base de datos esa carrera
		$arrayNombresCarreras = [];
		try{
			$consultaNombres = $pdo->prepare("select nombre from carreras");
			$consultaNombres->execute();
			while($fila=$consultaNombres->fetch()){
				array_push($arrayNombresCarreras,trim(strtolower($fila[0])));
			}
		}catch(PDOException $e){
			echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
		}

		/*echo $nombreCarrera;
		echo print_r($arrayNombresCarreras);*/

		if(in_array(trim($nombreCarrera),$arrayNombresCarreras)){
			echo "errorcarreraexistente";
		}else{//SI NO EXISTE HAY QUE AÑADIRLA
		

			try{

				$nombreCarrera = ucwords($nombreCarrera);
				$lugarCarrera = ucwords($lugarCarrera);


				$stmt=$pdo->prepare("insert into carreras(nombre,lugar,fecha) values (?,?,?)");
				$stmt->bindParam(1,$nombreCarrera);
				$stmt->bindParam(2,$lugarCarrera);
				$stmt->bindParam(3,$fechaCarrera);

				$stmt->execute();
				echo "carreraregistrada";

			}catch(PDOException $e){

			}
		}

}//FIN CREAR CARRERA




//MODIFICAR CARREARA (buscadorcarrerasadmin.php)

if(isset($_POST["nombrecarreramod"])&&isset($_POST["lugarcarreramod"])&&isset($_POST["fechacarreramod"])){
		
	$nombre = $_POST["nombrecarreramod"];
	$lugar = $_POST["lugarcarreramod"];
	$fecha =$_POST["fechacarreramod"];
	
	$idOriginal = $_POST["idOriginal"];
	
	//SI LOS CAMPOS ESTAN VACIOS
	if ($nombre==""||$lugar==""||$fecha==""){
		echo "NOMBREVACIO";
	}else{

		$nombre = ucwords($nombre);
		$lugar = ucwords($lugar);


		//COMPROBAR SI EXISTE ALGUN CORREO IGUAL AL INTRODUCIDO
		$arraycarreras=[];
		$existe=0;	
		try{
			$consulta = $pdo->query("select * from carreras where nombre='$nombre'");		
			while($row = $consulta->fetch()){
				array_push($arraycarreras,$row);
			}
			$existe=count($arraycarreras);			
		}catch(PDOException $e){
			
		}

		
		//Obtener el nombre actual del usuario a modificar 
		//(Esto se utilizara en las comprobaciones, ya que si el correo del usuario actual es igual 
		//al mismo correo del usuario actual, no deberia de dar error, pero si es igual al correo de otro usuario si)
		$consulta1 = $pdo->query("select nombre from carreras where idcarrera='$idOriginal'");
		while($row = $consulta1->fetch()){
			$nombreActual = $row['nombre'];			
		}



		//Si el correo introducido es "admin@admin.com":
		
		if($existe==0||$existe==1&&$nombre==$nombreActual&&$nombre!="") {//Si no es "admin@admin.com y no existe en la BD:
			try{
				$stmt=$pdo->prepare("update carreras set nombre='$nombre', lugar='$lugar',fecha='$fecha' where idcarrera='$idOriginal'");
				$stmt->execute();
				echo "carreramodificada";
			}catch(PDOException $e){
				echo "errormodificarcarrera";
			}
		}else if($existe==1 &&$nombre!==$nombreActual&&$nombre==""){
			echo "erroradmin";
		}else{
			echo "erroradmin";
		}

	}//FIN SI LOS CAMPOS ESTAN VACIOS

}//FIN MODIFICAR CARRERA

//ELIMINAR CARRERA (buscadorcarrerasadmin.php)
if(isset($_POST["idcarrera"])){ //TENGO QUE NEGAR LA SUBIDA DE LA FOTO, SI NO CUANDO SUBO UNA FOTO Y LE DOY A GUARDAr, COMO TAMBIEN UTILIZO EL ID, SE BORRA.
	//echo $_POST["idusuario"]; //ID DEL USUARIO

	try{
		$consultaCorreos=$pdo->prepare("delete from carreras where idcarrera=".$_POST["idcarrera"]);
		$consultaCorreos->execute();                   
		
		echo "carreraeliminada";

	}catch(PDOException $e){
		//echo "ERROR EN LA SENTENCIA SQL ".$e->getMessage();
	}
}//FIN ELIMINAR CARRERA









?>
