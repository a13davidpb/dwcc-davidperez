<?php
    session_start();    
    /// INFORMACION DE INICIO DE SESION ///
    if(isset($_SESSION["usuario"])){
	
        //echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";
        //SI ENTRAMOS COMO ADMINISTRADOR:
        if($_SESSION["usuario"]=="admin@admin.com"){
            require "cabeceraadmin.php";	
        }else{
            require "cabecerausuario.php";
        }
    }else{
        require "cabecera.php";
    }
    ////////////////////////////////////////


    echo '<div id="listado">';
  
    require_once 'conexion.php';

    try{
        $stmt=$pdo->prepare("select * from carreras order by idcarrera");
        

    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Ejecutamos consulta
        $stmt->execute();

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        echo '<table border="1">
                <tr>
                    <th>Nombre</th>
                    <th>Lugar</th>
                    <th>Fecha</th>
                    <th>Inscritos</th>
                    <!--<th>Gestión</th>-->
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   
           // echo "{$fila['idcarrera']}";

           $idcarrera = "{$fila['idcarrera']}";           
     
           $stmt1 = $pdo->prepare("select count(idinscripcion) from inscripciones where idcarrerainscripcion=$idcarrera");
           $stmt1->setFetchMode(PDO::FETCH_ASSOC);
           $stmt1->execute();

           $fila1=$stmt1->fetch();
                       
            echo "<tr>
                    <td> {$fila['nombre']}</td>
                    <td> {$fila['lugar']}</td>
                    <td> {$fila['fecha']}</td>
                    <td> {$fila1["count(idinscripcion)"]} </td>
                   <!-- <td><a href='editar.php?id={$fila['idcarrera']}'>Editar</a> | <a href='bajas.php?id={$fila['idcarrera']}'> Borrar</a></td></tr>-->";

        }
        $cont++;
        echo '</table>';
    } else 
        echo 'No se han encontrado registros de carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";


    require 'pie.php';
  ?>