<?php
    session_start();    

    require "cabecerausuario.php";	
    echo "<br>";
    echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";    

    echo '<div id="listado">';
  
    require_once 'conexion.php';


    $correoUsuarioLogeado = $_SESSION["usuario"];

    $consultaID = $pdo->query("select idusuario from usuarios where correo='$correoUsuarioLogeado'");
    $consultaID->execute();
    while($row = $consultaID->fetch()){
        $idUsuarioLogeado = $row['idusuario'];			
    }
   


    try{
        $stmt=$pdo->prepare("select * from inscripciones where idusuarioinscripcion='$idUsuarioLogeado'");  
    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        //Ejecutamos consulta
        $stmt->execute();
      

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        echo '<table border="1">
                <tr>
                    <th>DORSAL</th>
                    <!--<th>idusuarioinscripcion</th>-->
                    <th>Nombre de la Carrera</th>
                   
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   

                $idCarreraApuntado = $fila['idcarrerainscripcion'];
                //Consulta para saber el nombre de la carrera
                $stmt1=$pdo->prepare("select nombre from carreras where idcarrera='$idCarreraApuntado'");
                $stmt1->setFetchMode(PDO::FETCH_ASSOC);
                $stmt1->execute();

                //En el siguiente if inserto varias consultas distintas:
                if($fila1=$stmt1->fetch()){               
                    echo "<tr>
                            <td> {$fila['idinscripcion']}</td>
                            <!--<td> {$fila['idusuarioinscripcion']}</td>-->
                            <td> {$fila1['nombre']}</td>
                        </tr>";
                }
        }
        $cont++;
        echo '</table>';
    } else 
        echo 'No estás apuntado a ninguna carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";


    require 'pie.php';
  ?>