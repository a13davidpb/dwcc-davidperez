<?php
    session_start();    
    
    
    
    require "cabeceraadmin.php";	
    
    echo "<br>";
    echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";    

  
  
    require_once 'conexion.php';
  
    echo"<br><br>";
    echo "<strong>Nombre de la carrera: </strong>&nbsp;&nbsp;";
    echo "<input type='text' name='carrera' id='carrera'/>";
    echo '&nbsp;&nbsp;<input type="button" id="buscar" name="buscar" value="Obtener Información" />';
    echo '<div style="margin-top:1cm" id="mensaje"></div>';
    echo '<div style="margin:auto;margin-top:1cm;width:6cm;" id="zonaresultado"></div>';




    
     echo '<div id="listado">';
    try{
        $stmt=$pdo->prepare("select * from carreras order by idcarrera");
        

    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Ejecutamos consulta
        $stmt->execute();

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        echo '<table class="listadocarreras"id="listadocarreras" style="width:18cm;margin-left:-2cm" border="1">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Lugar</th>
                    <th>Fecha</th>
                    <!--<th>Inscritos</th>-->
                    <th>Accion</th>
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   
           // echo "{$fila['idcarrera']}";

           $idcarrera = "{$fila['idcarrera']}";           
     
           $stmt1 = $pdo->prepare("select count(idinscripcion) from inscripciones where idcarrerainscripcion=$idcarrera");
           $stmt1->setFetchMode(PDO::FETCH_ASSOC);
           $stmt1->execute();

           $fila1=$stmt1->fetch();
                       
            echo "<tr>
                    <td> {$fila['idcarrera']}</td>
                    <td> {$fila['nombre']}</td>
                    <td> {$fila['lugar']}</td>
                    <td> {$fila['fecha']}</td>
                    <!--<td> {$fila1["count(idinscripcion)"]} </td>-->
                    <td> 
                        <a class='btneliminar' type='button' value='eliminar' style='border:0' name='boton'>Eliminar</a><br>
                        <a class='btnmodificar' type='button' value='modificar' style='border:0' name='boton'>Modificar</a><br>                        
                    </td>";

        }
        $cont++;
        echo '</table>';
    } else 
        echo 'No se han encontrado registros de carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";
    
    
    
  ?>
        
    <form id="crearCarreraAdmin">
    <strong>CREAR NUEVA CARRERA</strong><br><br>
       <span style="background-color:none;padding-right:60px;">Nombre:</span><input style="width:4cm" id="nombrenuevacarrera" name="nombrenuevacarrera" type="text"><br>
       <span style="background-color:none;padding-right:71px;margin-top:7px">Lugar:</span><input style="width:4cm;margin-top:7px" id="lugarnuevacarrera" name="lugarnuevacarrera" type="text"><br>
       <span style="background-color:none;padding-right:26px;margin-top:7px">Fecha Carrera:</span><input style="width:4cm;margin-top:7px" id="fechanuevacarrera"  name="fechanuevacarrera" type="date"><br>
       <br>
       <input id="botonCrearCarrera"type="button" id="botonCrearCarrera" class="btn"value="crear carrera" style="border:0" name="botonCrearCarrera">
    </form>
    <div style="margin-top:-5cm"id="mensajecrear"></div>




    <script src="js/jquery.js"></script>
    <script type="text/javascript">
            $(document).ready(function(){
                //Evitar browser cache
                $.ajaxSetup({cache:false});

                //AL HACER CLICK EN EL INPUT Y ESCRIBIR
                $("#carrera").click(function(){
                    //Oculto el listado
                   
                    $(this).val("");
                    $("#zonaresultado").html("").removeClass("zonaconborde");


                    //Cada vez que pulsamos una tecla debe ocurrir la sugerencia
                    $("#carrera").keyup(function(){

                        var nombreCarreraBuscar = $.trim($(this).val());//->String
                        //Creo una lista de sugerencias:
                        $.get("crud.php",{nombreCarreraSugerir:nombreCarreraBuscar},function(datosDevueltos){
                        
                            if(datosDevueltos!=""){
                                $("#zonaresultado").html(datosDevueltos).addClass("zonaconborde");
                            }else{
                                $("#zonaresultado").html(datosDevueltos).removeClass("zonaconborde");
                            };

                            //Por cada elemento de la lista:
                            $("#zonaresultado li").each(function(){
                                
                                //Al pasar el raton por encima
                                $(this).mouseover(function(){
                                    $(this).addClass("enlace_sugerencia_over");
                                });
                                //Al sacar el raton de encima
                                $(this).mouseout(function(){
                                    $(this).removeClass("enlace_sugerencia_over");
                                });

                                //Cuando pincho en uno:
                                $(this).click(function(){
                                    var carreraSeleccionada = $(this).text();

                                    //Copiar la carrera en la caja de texto
                                    $("#carrera").val(carreraSeleccionada);

                                    //Vacio la consulta
                                    $("#zonaresultado").html("").removeClass("zonaconborde");                                    
                                });//Fin cuando se pincha en uno

                            });//Fin cada elemento
                        });//FIN DE LA SUGERENCIA                                       
                     });//Fin cada vez que se escribe
                });//FIN CLICK INPUT

                //BOTON BUSCAR "Obtener informacion":
                $("#buscar").click(function(){
                    $("#listado").hide();
                    $("#crearCarreraAdmin").hide();


                    //Si no esta vacio el campo de escribir carrera:
                    var carreraBuscar = $.trim($("#carrera").val());

                    

                    if(carreraBuscar!=""){
                        $.get("crud.php",{nombreCarreraBuscar:carreraBuscar},function(result){
                            //console.log(result.status);

                            if(result.status=="carreranoencontrada"){//Si no se encuentra la carrera
                                $("#mensaje").addClass("error").text("No existe la carrera buscada.").fadeIn(1000).delay(500).fadeOut(2000);
                            }else{
                                

                                /*console.log(result);
                                var cadena="";
                                for(clave in result){
                                    cadena += result[clave]+"<br>";
                                }*/

                                var arrayCampos = [];

                                for(var i =0;i<4;i++){
                                    arrayCampos.push(result[i]);
                                }

                                $("#zonaresultado").html('<table class="listadocarreras" style="margin-left:-5cm;width:17cm" border="1"><tr><th>ID</th><th>Nombre</th><th>Lugar</th><th>Fecha</th><th>Accion</th></tr><tr><td>'+arrayCampos[0]+'</td><td>'+arrayCampos[1]+'</td><td>'+arrayCampos[2]+'</td><td>'+arrayCampos[3]+'</td><td><a class="btneliminar" type="button" value="eliminar" style="border:0" name="boton">Eliminar</a><br><a class="btnmodificar" type="button" value="modificar" style="border:0" name="boton">Modificar</a><br></td></tr></table>');


                                //Llamo a lsa funcion modificar y eliminar carrera 
                                //(En este punto solo afecta a la carrera buscada)
                                modificarCarrera();
                                eliminarCarrera();


                                
                            }
                        });
                    }

                })//FIN BOTON BUSCAR





                 //CREAR UNA NUEVA CARRERA (Copiado de registrarse.php)
                //EL BOTON NO DEBE SER SUBMIT
                $("#botonCrearCarrera").click(function(event){
                        //Llamo a la funcion
                        crearCarrera() ;
                });   
                    //Crear el usuario dandole a intro:
                $("input").keyup(function(evento){
                    if(evento.which==13){
                        //Llamo a la funcion
                        crearCarrera();
                    }
                });

                //FUNCION PARA CREAR CARRERA
                function crearCarrera(){
                    var nombreNuevaCarrera = $.trim($("#nombrenuevacarrera").val());
                    var lugarNuevaCarrera = $.trim($("#lugarnuevacarrera").val());
                    var fechaNuevaCarrera = $.trim($("#fechanuevacarrera").val());

                    //Pongo a mayusculas las primeras letras de cada nombre
                    var arrayNombre = nombreNuevaCarrera.split(" ");
                    for(var i=0;i<arrayNombre.length;i++){
                        arrayNombre[i] = arrayNombre[i].charAt(0).toUpperCase()+arrayNombre[i].slice(1);
                    }
                    nombreNuevaCarrera = arrayNombre.join(" ");
                    //Pongo a mayusculas las primeras letras de cada apellido
                    var arrayLugar = lugarNuevaCarrera.split(" ");
                    for(var i=0;i<arrayLugar.length;i++){
                        arrayLugar[i] = arrayLugar[i].charAt(0).toUpperCase()+arrayLugar[i].slice(1);
                    }
                    lugarNuevaCarrera = arrayLugar.join(" ");

                    if( nombreNuevaCarrera == "" || lugarNuevaCarrera == ""|| fechaNuevaCarrera == ""){//SI LOS CAMPOS ESTAN VACIOS
                            $("#mensajecrear").addClass("error").text("No pude haber campos Vacios.").fadeIn(1000).delay(500).fadeOut(2000);
                    }else{
                        $.post("crud.php",{newnomcar:nombreNuevaCarrera,newlugcar:lugarNuevaCarrera,newfeccar:fechaNuevaCarrera},function(datoDevuelto){
                            if(datoDevuelto=="errorcarreraexistente"){
                                $("#mensajecrear").addClass("error").text("Esa carrera ya está registrada.").fadeIn(1000).delay(500).fadeOut(2000);
                            }else if(datoDevuelto="carreraregistrada"){
                                //$("#mensajecrear").addClass("bien").text("Carrera registrada.").fadeIn(1000).delay(500).fadeOut(2000);
                                location.reload();                                 
                            }
                        });
                    }
                }//FIN FUNCION CREAR CARRERA

                //Llamo a lsa funcion modificar y eliminar carrera
                modificarCarrera();
                eliminarCarrera();

                //Funcion que modifica la carrera actual
                function modificarCarrera(){

                            //CADA VEZ QUE SE HACE CLICK EN UN BOTON "modificar":
                            $(".listadocarreras").on("click",".btnmodificar",function(){
                        //console.log("MODIFICAR");

                            //Si se quiere editar otro registo distinto al que está editando ahora
                                //Evitar dos ediciones a la vez!
                                if($("input.edicion").length!=0){
                                    padreTR.html(contenidoOriginalFila);
                                };

                            if(confirm('¿Desea editar esta carrera?')){	
                                //NECESITAMOS LLEGAR AL "tr"
                                padreTR = $(this).parent().parent(); //tr donde esta la imagen que hice el click
                                //Obtenemos el nombre del pais de la fila
                                idOriginal = padreTR.find("td:eq(0)").text();//->Esto es el nombre del usuario							
                                //Contenido original de la fila:(Se necesitara en un futuro cuando se seleccione otra fila)
                                contenidoOriginalFila=padreTR.html();

                                //Recorremos los td y convertimos el texto en cajas de texto
                                padreTR.find("td").slice(1,4).each(function(){
                                    //Creamos cajas de texto en cada campo de la fila
                                    $(this).html("<input class='selector edicion' style='width:2cm;' type='text' value='"+$(this).text().trim()+"'/>");//Con esto podremos editar la fila 
                                });

                                //Al pulsar Enter en alguna de las cajas de texto... realiza la consulta de edicion
                                $("input").keyup(function(evento){
                                

                                    if(evento.which==13){//Si pulsamos ENTER hacemos la peticion ajax...
                                        var datosEnviar={
                                            /*idusuariomod:$("input:eq(0)").val().trim(),*/
                                            nombrecarreramod:$(".selector:eq(0)").val(),
                                            lugarcarreramod:$(".selector:eq(1)").val(),
                                            fechacarreramod:$(".selector:eq(2)").val(),                                
                                            idOriginal:idOriginal                                
                                        };
                                        console.log(datosEnviar);

                                        //Hacemos la consulta a la base de datos
                                        $.post("crud.php",datosEnviar,function(respuesta){
                                        console.log(respuesta);
                                        
                                            //Salir de la edicion y mostrar durante 1 segundo la fila editada en color verde:
                                            if(respuesta == "carreramodificada"){
                                                //Sacamos los input y dejamos las celdas
                                                padreTR.find("td").slice(1,5).each(function(){
                                                    $(this).text($(this).find("input").val());
                                                });
                                                //Poner fondo VERDE durante un segundo
                                                padreTR.addClass('bien').delay(1000).queue(function(){
                                                    $(this).removeClass('bien');
                                                    location.reload();
                                                });//Fin BIEN
                                            }else if(respuesta == "errormodificarcarrera"){
                                                padreTR.addClass('mal').delay(1000).queue(function(){
                                                    $(this).removeClass('bien');
                                                });//Fin MAL
                                            }else if(respuesta =="erroradmin"){
                                                $("#mensaje").html("<strong style='color:#5c1521;'>ERROR AL MODIFICAR LA CARRERA.</strong>").fadeIn(5).delay(500).fadeOut(1000);
                                            }else if(respuesta == "NOMBREVACIO"){
                                                $("#mensaje").html("<strong style='color:#5c1521;'>NO PUEDE HABER CAMPOS VACÍOS.</strong>").fadeIn(5).delay(500).fadeOut(1000);
                                            }
                                        });//Fin POST edicion de datos  
                                    };
                                });//Fin pulsar enter edicion de datos
                                
                            };

                     });//FIN MODIFICAR CARRERA

                }//Fin funcion modificar carrera

                //Funcion para eliminar una carrera
                function eliminarCarrera(){
                        //CADA VEZ QUE SE HACE CLICK EN UN BOTON "eliminar":      
                        $(".listadocarreras").on("click",".btneliminar",function(){         
                            if(confirm("¿Desea borrar esta carrera?")){
                                padreTR = $(this).parent().parent();
                                //console.log(padreTR.children().first().text());
                            
                                var idEliminar = padreTR.find("td:eq(0)").text();

                                var datosEnviar = {idcarrera:idEliminar};
                                $.post("crud.php",datosEnviar, function(respuesta){
                                    //console.log(respuesta);
                                    if(respuesta=="carreraeliminada"){
                                        padreTR.addClass("mal").delay(1000).queue(function (){
                                            //Elimino de la pagina la fila
                                            $(this).remove();
                                            //Recargo pagina
                                            location.reload();
                                        });
                                    }

                                });

                            };
                        });//FIN ELIMINAR USUARIO
                }//Fin funcion





        });
    
    </script>