<?php
    session_start();    
    
    
    
    require "cabecerausuario.php";	
    
    echo "<br>";
    echo "Usted se ha identificado como: <strong>".$_SESSION["usuario"]."</strong>";    

  
  
    require_once 'conexion.php';
  
    echo"<br><br>";
    echo "<strong>Nombre de la carrera: </strong>&nbsp;&nbsp;";
    echo "<input type='text' name='carrera' id='carrera'/>";
    echo '&nbsp;&nbsp;<input type="button" id="buscar" name="buscar" value="Obtener Información" />';
    echo '<div class="mensajeprueba" style="margin-top:1cm" id="mensaje"></div>';
    echo '<div style="margin:auto;margin-top:1cm;width:6cm;" id="zonaresultado"></div>';
    echo '<div style="margin-top:1cm" id="mensaje2"></div>';



    
     echo '<div id="listado">';
    try{
        $stmt=$pdo->prepare("select * from carreras order by idcarrera");
        

    	//Formato de devolución de datos como array asociativo
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Ejecutamos consulta
        $stmt->execute();

        if($stmt->rowCount()!=0){
        //Mostramos el listado de registros
        echo '<table id="listadocarrerasapuntarse" border="1">
                <tr>
                    <th style="display:none">ID</th>
                    <th>Nombre</th>
                    <th>Lugar</th>
                    <th>Fecha</th>
                    <th>Accion</th>
                    <!--<th>Gestión</th>-->
                </tr>';

                $cont = 0;
        while($fila=$stmt->fetch())
        {   


                       
            echo "<tr>
                    <td style='display:none'>{$fila['idcarrera']}</td>
                    <td> {$fila['nombre']}</td>
                    <td> {$fila['lugar']}</td>
                    <td> {$fila['fecha']}</td>
                    <td> <a class='btnapuntarse' type='button' value='apuntarme' style='border:0' name='boton'>Apuntarme</a><br><a class='btndesapuntarse' type='button' value='apuntarme' style='border:0' name='boton'>Desapuntarme</a> </td>
                   <!-- <td><a href='editar.php?id={$fila['idcarrera']}'>Editar</a> | <a href='bajas.php?id={$fila['idcarrera']}'> Borrar</a></td></tr>-->";

        }
        $cont++;
        echo '</table>';
    } else 
        echo 'No se han encontrado registros de carrera.';


    } catch (PDOException $e) {
        echo "Error en la consulta SQL.";
        
    }

    echo "</div>";
    
    
    
  ?>

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
            $(document).ready(function(){
                //Evitar browser cache
                $.ajaxSetup({cache:false});

                //AL HACER CLICK EN EL INPUT Y ESCRIBIR
                $("#carrera").click(function(){
                    

                    $(this).val("");
                    $("#zonaresultado").html("").removeClass("zonaconborde");


                    //Cada vez que pulsamos una tecla debe ocurrir la sugerencia
                    $("#carrera").keyup(function(){

                        var nombreCarreraBuscar = $.trim($(this).val());//->String
                        //Creo una lista de sugerencias:
                        $.get("crud.php",{nombreCarreraSugerir:nombreCarreraBuscar},function(datosDevueltos){
                        
                            if(datosDevueltos!=""){
                                $("#zonaresultado").html(datosDevueltos).addClass("zonaconborde");
                            }else{
                                $("#zonaresultado").html(datosDevueltos).removeClass("zonaconborde");
                            };

                            //Por cada elemento de la lista:
                            $("#zonaresultado li").each(function(){
                                
                                //Al pasar el raton por encima
                                $(this).mouseover(function(){
                                    $(this).addClass("enlace_sugerencia_over");
                                });
                                //Al sacar el raton de encima
                                $(this).mouseout(function(){
                                    $(this).removeClass("enlace_sugerencia_over");
                                });

                                //Cuando pincho en uno:
                                $(this).click(function(){
                                    var carreraSeleccionada = $(this).text();

                                    //Copiar la carrera en la caja de texto
                                    $("#carrera").val(carreraSeleccionada);

                                    //Vacio la consulta
                                    $("#zonaresultado").html("").removeClass("zonaconborde");                                    
                                });//Fin cuando se pincha en uno

                            });//Fin cada elemento
                        });//FIN DE LA SUGERENCIA                                       
                     });//Fin cada vez que se escribe
                });//FIN CLICK INPUT

                //BOTON BUSCAR "Obtener Información":
                $("#buscar").click(function(){
                    //Oculto el listado
                    $("#listado").hide();

                    //Si no esta vacio el campo de escribir carrera:
                    var carreraBuscar = $.trim($("#carrera").val());

                    

                    if(carreraBuscar!=""){
                        $.get("crud.php",{nombreCarreraBuscar:carreraBuscar},function(result){
                            //console.log(result.status);

                            if(result.status=="carreranoencontrada"){//Si no se encuentra la carrera
                                $("#mensaje").addClass("error").text("No existe la carrera buscada.").fadeIn(1000).delay(500).fadeOut(2000);
                            }else{
                                

                                /*console.log(result);
                                var cadena="";
                                for(clave in result){
                                    cadena += result[clave]+"<br>";
                                }*/

                                var arrayCampos = [];

                                for(var i =0;i<4;i++){
                                    arrayCampos.push(result[i]);
                                }

                                //<a class='btnmodificarfoto' type='button' value='editarfoto' style='border:0' name='boton'>Apuntarme</a>
                                $("#zonaresultado").html('<table style="margin-left:-5cm;width:17cm" border="1"><tr><th style="display:none">ID</th><th>Nombre</th><th>Lugar</th><th>Fecha</th><th>Accion</th></tr><tr><td style="display:none">'+arrayCampos[0]+'</td><td>'+arrayCampos[1]+'</td><td>'+arrayCampos[2]+'</td><td>'+arrayCampos[3]+'</td><td><a class="btnapuntarse" type="button" value="apuntarme" style="border:0" id="apuntarme" name="boton">Apuntarme</a><br><a class="btndesapuntarse" type="button" value="apuntarme" style="border:0" id="desapuntarme" name="boton">Desapuntarme</a></td></tr></table>');
                                

                                //CUANDO HACES CLICK EN "Apuntarme"
                                $("#apuntarme").on("click",function(){
                                    var idCarreraApuntarme = result[0];
                                    $.get("crud.php",{idCarreraApuntarme:idCarreraApuntarme},function(resultado){
                                        //console.log(resultado);

                                        if(resultado=="inscritoencarrera"){
                                            $("#mensaje2").addClass("bien").text("Te has inscrito en la carrera.").fadeIn(1000).delay(500).fadeOut(2000);
                                        }else if(resultado=="errorinscribirse"){
                                            $("#mensaje2").removeClass("bien");
                                            $("#mensaje2").addClass("error").text("No puedes inscribirte.").fadeIn(1000).delay(500).fadeOut(2000);
                                        }
                                    });
                                });//FIN APUNTARME

                                //CUANDO HACES CLICK EN "Desapuntarme
                                $("#desapuntarme").on("click",function(){
                                    var idCarreraDesapuntarme = result[0];
                                    $.get("crud.php",{idCarreraDesapuntarme:idCarreraDesapuntarme},function(resultado){
                                        //console.log(resultado);
                                        if(resultado=="desapuntadocarrera"){
                                            $("#mensaje2").addClass("bien").text("Te has desapuntado de la carrera.").fadeIn(1000).delay(500).fadeOut(2000);
                                        }else if(resultado=="errordesapuntarse"){
                                            $("#mensaje2").removeClass("bien");
                                            $("#mensaje2").addClass("error").text("No puedes desapuntarte.").fadeIn(1000).delay(500).fadeOut(2000);
                                        }
                                    });
                                });//FIN DESAPUNTARME
                            };

                        });//Fin get                       
                    }//Fin if busqueda =""  

                });//FIN BUSCAR    



                 //APUNTARSE EN UNA CARRERA DE LA LISTA GRANDE (Sin buscar)   
                $("#listadocarrerasapuntarse").on("click",".btnapuntarse",function(){         
                    padreTR = $(this).parent().parent();
                    var idCarreraApuntarme  = padreTR.find("td:eq(0)").text();
                    $.get("crud.php",{idCarreraApuntarme:idCarreraApuntarme},function(resultado){
                        //console.log(resultado);

                        if(resultado=="inscritoencarrera"){
                            $("#mensaje").addClass("bien").text("Te has inscrito en la carrera.").fadeIn(1000).delay(500).fadeOut(2000);
                        }else if(resultado=="errorinscribirse"){
                            $("#mensaje").removeClass("bien");
                            $("#mensaje").addClass("error").text("No puedes inscribirte.").fadeIn(1000).delay(500).fadeOut(2000);
                        }
                    });
                });//FIN APUNTARSE EN UNA CARRERA DE LA LISTA GRANDE (Sin buscar)

                //DESAPUNTARSE DE UNA CARRERA DE LA LISTA GRANDE (Sin buscar)
                $("#listadocarrerasapuntarse").on("click",".btndesapuntarse",function(){         
                    padreTR = $(this).parent().parent();
                    var idCarreraDesapuntarme  = padreTR.find("td:eq(0)").text();
                    $.get("crud.php",{idCarreraDesapuntarme:idCarreraDesapuntarme},function(resultado){
                        //console.log(resultado);
                        if(resultado=="desapuntadocarrera"){
                            $("#mensaje").addClass("bien").text("Te has desapuntado de la carrera.").fadeIn(1000).delay(500).fadeOut(2000);
                        }else if(resultado=="errordesapuntarse"){
                            $("#mensaje").removeClass("bien");
                            $("#mensaje").addClass("error").text("No puedes desapuntarte.").fadeIn(1000).delay(500).fadeOut(2000);
                        }
                    });
                });





              



            });
    
    </script>