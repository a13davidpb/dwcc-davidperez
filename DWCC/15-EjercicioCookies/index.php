<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cookies David Perez</title>
    <style>
        body{
            background-color:rgb(221, 191, 218);
        }
        input{
            margin:20px;
        }
        div{
            margin-left:20px;
            display:none;
        }
        #btnborrarcookies{
            display:none;
        }
        #btnvercookies{
            display:none;
        }
        #btnvercookiesphp{
            display:none;
        }
    </style>
</head>
<body>
    <form>
        <input id="telefono" type="text" placeholder="Telefono"/><br>
        <input id="nombre" type="text" placeholder="Nombre"/><br>
        <input id="email" type="text" placeholder="Email"/><br>
        <input id="btnenviar" type="button" value="Enviar"/>
        <input id="btnborrarcookies" type="button" value="Borrar Cookies"/>
        <input id="btnvercookies" type="button" value="Ver Cookies JavaScript"/>
        <input id="btnvercookiesphp" type="button" value="Ver Cookies Php"/>
    </form>


    <script src="./js/jquery.js"></script>
    <script>
        
        $(document).ready(function(){//INICIO DOCUMENT READY
            //Cada vez que inicio la pagina, borro el contenido de los campos
            $("#telefono").val("");
            $("#nombre").val("");
            $("#email").val("");
            console.log(document.cookie);

            //Declaro un array con las cookies existentes en la páginas:
            var arrayCookie = document.cookie.split(";");
         

            //Si la longitud del array crado es mayor a 0, quiere decir que hay datos en las cookies:
            if(document.cookie.length>0){
                $("input").hide();
                //Muestro los botones y el contador de visitas:
                $("#btnborrarcookies").show();
                $("#btnvercookies").show();
                $("#btnvercookiesphp").show();
                $("div").show();

                //Oculto el boton enviar:
                $("#btnenviar").hide(); 

            }           

            //CREAR COOKIES:
            //Cuando se hace click en el boton enviar:
            $("#btnenviar").on("click",function(event){                

                if($("#telefono").val()!=""&&$("#nombre").val()!=""&&$("#email").val()!=""){
                    $("input").hide();    
                    //console.log($("#telefono").val())
                    //Muestro los botones:                
                    $("#btnborrarcookies").show();
                    $("#btnvercookies").show();
                    $("#btnvercookiesphp").show();
                    $("div").show();

                    //Oculto el boton enviar:
                    $("#btnenviar").hide();
                    
                    //Llamo a la funcion crear cookies
                    crearCookie(event);
                 
                }

            });//FIN CREAR COOKIES

            //BORRAR COOKIES
            //Cuando se hace click en el boton borrar cookies
            $("#btnborrarcookies").on("click",function(event){
                $("input").show();
                //Muestro el boton enviar otra vez:
                $("#btnenviar").show();
                //Oculto el boton de borrar
                $("#btnborrarcookies").hide();
                $("#btnvercookiesphp").hide();

                //Llamo a la funcion borrar cookies
                borrarCookie(event);

            });//Fin borrar cookies


            //VER TODAS LAS COOKIES
            //Cuando se hace click en el boton ver cookies
            $("#btnvercookies").on("click",function(){
                alert(document.cookie);
            });//Fin ver cookies   

             //VER TODAS LAS COOKIES EN PHP
            //Cuando se hace click en el boton ver cookies
            $("#btnvercookiesphp").on("click",function(){
                if($("#telefono").val()!=""&&$("#nombre").val()!=""&&$("#email").val()!=""){
                    window.location.href="pagina.php";
                }
            });//Fin ver cookies   


            //Funcion que crea las cookies (Se usa cuando se envia el formulario):
            function crearCookie(e){              
                
                //console.log(e.target.id);

                //CREO LAS COOKIES:
                //Cookie para el telefono:
                if($("#telefono").val()!=""){
                    document.cookie="telefono = "+$("#telefono").val().trim();
                }
                //Cookie para el nombre:
                if($("#nombre").val()!=""){
                    document.cookie="nombre ="+$("#nombre").val().trim();
                }
                //Cookie para el email:
                if($("#email").val()!==""){
                    document.cookie="email ="+$("#email").val().trim();
                }               
                               
                             
            }//Fin funcion crear cookies


            //Funcion que borra las cookies (Se usa al hacer click en el boton borrar)
            function borrarCookie(e){
                //console.log(e.target.id);
                //Si el boton seleccionado es "btnborrarcookies" se borran:
                if(e.target.id=="btnborrarcookies"){
                    document.cookie = "telefono =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "nombre =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "email =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "visitas =;expires=Thu, 01 Jan 1970 00:00:00 UTC;"
                }

            }//Fin funcion que borra las cookies


        });//FIN DOCUMENT READY
    
    </script>


    
</body>
</html>