<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cookies desde PHP</title>
    <style>
        body{
            background-color:rgb(221, 191, 218);
        }
        input{
            margin:20px;
        }

    </style>
</head>
<body>
    <?php   
        if ( isset( $_COOKIE['visitas'] ) ) {

            setcookie( 'visitas', $_COOKIE[ 'visitas' ] + 1);
            $mensaje = 'Numero de visitas: '.$_COOKIE[ 'visitas' ];
        }
        else {
        
            setcookie( 'visitas', 1);
            $mensaje = 'Accediendo por primera vez';
        }
        
        echo "<br><b>".$mensaje."</b><br>";
        echo "<br><b>Telefono:</b> ".$_COOKIE["telefono"]."<br>";
        echo "<br><b>Nombre:</b> ".$_COOKIE["nombre"]."<br>";
        echo "<br><b>Email:</b> ".$_COOKIE["email"]."<br>";


        echo "<br><br><b> Array de Cookies en PHP:</b><br>";
        print_r($_COOKIE);

    ?>

    <br>
    <input id="btnborrarcookies" type="button" value="Borrar Cookies"/>

    <script src="./js/jquery.js"></script>
    <script>
        console.log(document.cookie);


            //BORRAR COOKIES
            //Cuando se hace click en el boton borrar cookies
            $("#btnborrarcookies").on("click",function(event){
                $("input").show();
                //Muestro el boton enviar otra vez:
                $("#btnenviar").show();
                //Oculto el boton de borrar
                $("#btnborrarcookies").hide();

                //Llamo a la funcion borrar cookies
                borrarCookie(event);

            });//Fin borrar cookies



           //Funcion que borra las cookies (Se usa al hacer click en el boton borrar)
           function borrarCookie(e){
                //console.log(e.target.id);
                //Si el boton seleccionado es "btnborrarcookies" se borran:
                if(e.target.id=="btnborrarcookies"){
                    document.cookie = "telefono =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "nombre =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "email =;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
                    document.cookie = "visitas =;expires=Thu, 01 Jan 1970 00:00:00 UTC;"
                }
                window.location.href="index.php";
            }//Fin funcion que borra las cookies
    </script>
    
</body>
</html>