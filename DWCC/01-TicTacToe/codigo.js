
    var tablero = [];
  
    var letra = 'X';
    var tablero = [
                    ['X','O','X'],
                    ['O','X','O'],
                    ['X','O','O']
                  ];

    tresEnRaya(tablero,letra);

    var tamTabla = 3;
    var idDiv = 'divTablero';
    dibujaTablaEnDiv(tamTabla,idDiv);

    //Debemos definir la funcion para dibujar la tabla:
    function dibujaTablaEnDiv(tam,id){
          var table,caption,tbody,tr,th;

          
          table = document.createElement('table');          
          caption=document.createElement('caption');          
          caption.appendChild(document.createTextNode('Tres en Raya de '+tam+" x "+tam));
          table.appendChild(caption);

          tbody = document.createElement('tbody');
          table.appendChild(tbody);
          tr = document.createElement('tr');
          td = document.createElement('td');
          for(var i=0;i<tam;i++){
            tr=tr.cloneNode(false);
            tbody.appendChild(tr);
            for(var j=0;j<tam;j++){
              td=td.cloneNode(false);

              //Limpio el atributo class:
              td.removeAttribute('class');
              //Añado la clase a cada celda:
              td.classList.add('fila:'+i,'col:'+j);
              
              //Añadir un evento: (Al hacer click, llamamos a la funcion enJuego());
              td.addEventListener('click',enJuego);

              td.innerHTML = '-';
              tr.appendChild(td);
            }
          }
          document.getElementById(id).appendChild(table);
    }



    //Funcion enJuego() -> Esta funcion se ejecutara cada vez que se haga click;
    function enJuego(){

        //Miramos si en la anterior jugada ya gano alguien:
        const iLabel=document.getElementById('turno').textContent.split(' ')[0][0]; //Pilla la primera letra del campo "jugador"
        console.log("ILABEL "+iLabel);
        if(iLabel=='G'||iLabel=='E'){
          location.reload();


        }else{

            var ganador=false;  
        
            var letraCasilla = this.innerHTML;

         /*   console.log(letraCasilla);      // - o O
            console.log(this.classList[0]); // fila:0
            console.log(this.classList[1]); // col:0
          */
            //JUGADOR ACTUAL -> Se pilla de la etiqueta html del principio
            var letraJugador = document.getElementById('turno').textContent.split(' ')[1];
            console.log("Jugador: "+letraJugador);
            //Cambiamos letra de la casilla si la casilla contiene '-'
            if(letraCasilla=='-'){
              if(letraJugador=='X'){
                this.innerHTML = '<div style="color:#FF9A01">'+letraJugador+'</div>';
              }else{
                this.innerHTML = '<div style="color:#A20097">'+letraJugador+'</div>';
              }

              if(letraJugador=='X'){//Cambio de jugador
                document.getElementById('turno').innerHTML = 'Jugador <span style="color:#A20097">O</span>';
              }
              if(letraJugador=='O'){//Cambio de jugador
                document.getElementById('turno').innerHTML = 'Jugador <span style="color:#FF9A01">X</span>';
              }
            }




          //Crear un array con el tablero actual: 
      
            var tablero = [];
            var tablero = [ [],[],[] ];
            
          
            var array=[];
            for(var i=0;i<3;i++){
              for(var j=0;j<3;j++){
                //console.log("Contenido"+document.getElementsByClassName('fila:'+i+' col:'+j)[0].textContent);
                array.push(document.getElementsByClassName('fila:'+i+' col:'+j)[0].textContent);

              }
            }
            //console.log(array);

            tablero[0].push(array[0]);
            tablero[0].push(array[1]);
            tablero[0].push(array[2]);

            tablero[1].push(array[3]);
            tablero[1].push(array[4]);
            tablero[1].push(array[5]);

            tablero[2].push(array[6]);
            tablero[2].push(array[7]);
            tablero[2].push(array[8]);

            console.log(tablero);
            console.log(letraJugador);
            console.log("Ganador? "+tresEnRaya(tablero,letraJugador));

            var fin=tresEnRaya(tablero,letraJugador);
            if(fin==true){

              if(letraJugador=='X'){//Cambio de jugador
                document.getElementById('turno').innerHTML = '<span style="color:#006C12">Gana:</span> <span style="color:#FF9A01">X</span>';
              }
              if(letraJugador=='O'){//Cambio de jugador
                document.getElementById('turno').innerHTML = '<span style="color:#006C12">Gana:</span> <span style="color:#A20097">O</span>';
              }
            }
            
            //Comprobar si se puede seguir jugando
            var seguirJ = seguirJugando(tablero); //true/false
            if(seguirJ==true&&fin==false){
              console.log("Se puede seguir jugando");
            }else{
              console.log("No se puede seguir jugando");          
            }
        
            if(seguirJ==false&&fin==false){
              document.getElementById('turno').innerHTML = '<div style="color:blue">EMPATE'+'</div>';        
            }
        

  
        }



    }
    //Funcion que comprueba si se puede seguir jugando
    function seguirJugando(tablero){
      for(var i = 0;i<tablero.length;i++){
        for(var j = 0;j<tablero[i].length;j++){
          if(tablero[i][j]=='-'){
          //  if(tablero[i][j]!='X'&&tablero[i][j]!='O'){
            return true;
          }
        }
      }
      return false;
    }




    /////////////////////TRES EN RAYA/////////////////////////////////
    //Funcion tresEnRaya() -> Se ejecutara continuamente dentro de enJuego();
    function tresEnRaya(tablero,letra){  

      var gana = false;
      var fGana =false; //Comprobamos si gana por fila
      for(var i=0;i<3;i++){
          if(checkFila(tablero[i],letra)){
            fGana = true;
            break;
          }        
      }
     // console.log("Gana fila: "+fGana);


      var cGana = false;//Comprobamos si gana por columna
      for(i=0;i<tablero[0].length;i++){
          
          if(checkColumna(tablero,i,letra)){
            cGana = true;
            break;
          }

      }
      //console.log("Gana columna: "+cGana);


      var dIzDe = checkDeIzDe(tablero,letra); //Comprobamos si gana la Primera diagonal
      //console.log("Diagonal Iz/De: "+dIzDe);

      var dDeIz = checkDeDeIz(tablero,letra); //Comprobamos si gana la Segunda diagonal
      //console.log("Diagonal De/Iz: "+dDeIz);

      //Jugador gana?
      gana = fGana||cGana||dIzDe||dDeIz;

      if(gana==true){
        return gana;
      }else{
        return false;
      }

      
      

    }

    /////////////////////COMPROBACIONES DE TABLERO////////////////////////////////

    //Funcion que va comprobando cada fila -> Se usa en tresEnRaya()
    function checkFila(aFila,letra){
      for(var i=0;i<aFila.length;i++){
        if(aFila[i]!=letra){
          return false;
        }
      }
      return true;        
    }////////////////////////////////// FIN checkFila()

    //Funcion que va comprobando cada columna -> Se usa en tresEnRaya()
    function checkColumna(tablero,iColumna,letra){
      for(var i=0;i<tablero.length;i++){
        if(tablero[i][iColumna]!=letra){
          return false;
        }
      }
      return true;
    }////////////////////////////////// FIN checkColumna()

    //Comprueba primera diagonal -> Se usa en tresEnRaya()
    function checkDeIzDe(tablero,letra){
        for(var i=0;i<tablero.length;i++){
          if(tablero[i][i]!=letra){
            return false;
          }
        }
        return true;
    }////////////////////////////////// FIN checkDeIzDe()

    //Comprueba segunda diagonal -> Se usa en tresEnRaya()
    function checkDeDeIz(tablero,letra){
      const tamT = tablero.length;
      /*for(i=0,j=tamT;i<tamT;i++,j--){
        if(tablero[i][j]!=letra){
          return false;
        }
      }*/

      for(i=0;i<tamT;i++){
        if(tablero[i][tamT-1-i]!=letra){
          return false;
        }
      }
      return true;
    }//////////////////////////////////
    
